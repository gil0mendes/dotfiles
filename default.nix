{ config, inputs, lib, pkgs, ... }:

with lib;
with lib.my;
{
  imports =
    # I use home-manager to deploy files to $HOME and some other things
    [
      inputs.home-manager.nixosModules.home-manager
    ]
    # All my personal modules
    ++ (mapModulesRec' (toString ./modules) import);

  # Common config for all nixos machines; and to ensure the flake operates soundly
  environment.variables.DOTFILES = config.dotfiles.dir;
  environment.variables.DOTFILES_BIN = config.dotfiles.binDir;

  # configure nix and nixpkgs
  environment.variables.NIXPKGS_ALLOW_UNFREE = "1";
  nix =
    let
      filteredInputs = filterSelf inputs;
      nixPathInputs = mapAttrsToList (n: v: "${n}=${v}") filteredInputs;
      registry = mapAttrs (_: v: { flake = v; }) filteredInputs;
    in
    {
      package = pkgs.nixFlakes;
      extraOptions = "experimental-features = nix-command flakes";
      nixPath = nixPathInputs ++ [
        "nixpkgs-overlays=${config.dotfiles.dir}/overlays"
        "dotfiles=${config.dotfiles.dir}"
      ];
      settings = {
        substituters = [
          "https://aseipp-nix-cache.global.ssl.fastly.net"
          "https://nix-community.cachix.org"

          "https://nixpkgs-unfree.cachix.org/"
          "https://cuda-maintainers.cachix.org"

          # Binary Cache for Haskell.nix
          "https://cache.iog.io"
          "https://cache.zw3rk.com"
        ];
        trusted-public-keys = [
          "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="

          "nixpkgs-unfree.cachix.org-1:hqvoInulhbV4nJ9yJOEr+4wxhDV4xq2d1DK7S6Nj6rs="
          "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E="

          # Binary Cache for Haskell.nix
          "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
          "loony-tools:pr9m4BkM/5/eSTZlkQyRt57Jz7OMBxNSUiMC4FkcNfk="
        ];
        auto-optimise-store = true;
      };

      registry = registry // { dotfiles.flake = inputs.self; };

      # do some monthly cleanup
      gc = {
        automatic = true;
        dates = "monthly";
        options = "--delete-older-than 30d";
      };
    };

  # define the NixOS version to be installed
  system.configurationRevision = with inputs; mkIf (self ? rev) self.rev;
  system.stateVersion = mkDefault "23.05";

  # global tools to be available to all users
  environment.systemPackages = with pkgs; [
    dnsutils
    pciutils
    usbutils

    cached-nix-shell
    coreutils
    curl
    git
    wget
    gnumake
    unzip
    file

    neovim
    htop
    killall
    tree
		patchelf

    # Library for storing secrets securely in userspace
    libsecret
  ];
}

