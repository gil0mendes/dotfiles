{ config, options, lib, home-manager, ... }:

with lib;
with lib.my;
{
  options = with types; {
    user = mkOpt attrs { };

    dotfiles = {
      dir = mkOpt path
        (findFirst pathExists (toString ../.) [
          "${config.user.home}/.config/dotfiles"
          "/mnt/etc/dotfiles"
          "/etc/dotfiles"
          "/etc/nixos"
        ]);
      binDir = mkOpt path "${config.dotfiles.dir}/bin";
      configDir = mkOpt path "${config.dotfiles.dir}/config";
      modulesDir = mkOpt path "${config.dotfiles.dir}/modules";
    };

    home = {
      file = mkOpt' attrs { } "Files to place directly in $HOME";
      configFile = mkOpt' attrs { } "Files to place in $XDG_CONFIG_HOME";
      dataFile = mkOpt' attrs { } "Files to place in $XDG_DATA_HOME";

      programs = mkOpt' attrs { } "user programs";
      services = mkOpt' attrs { } "user services";
    };

    env = mkOption {
      type = attrsOf (oneOf [ str path (listOf (either str path)) ]);
      apply = mapAttrs
        (n: v:
          if isList v
          then concatMapStringsSep ":" (x: toString x) v
          else (toString v));
      default = { };
      description = "TODO";
    };
  };

  config = {
    # define the default user
    user =
      let
        user = builtins.getEnv "USER";
        name = if elem user [ "" "root" ] then "gil0mendes" else user;
      in
      {
        inherit name;
        description = "Gil Mendes";
        isNormalUser = true;
        home = "/home/${name}";
        uid = 1000;
        group = "users";
        # we don't needs the <wheel> group since we use a special <doas> rule.
        extraGroups = [
          "wheel"
          "adbuser"
          "networkmanager"
          "input"
          "uinput"
          "systemd-journal"
        ];
      };

    # install user packages to /etc/profiles instead. necessary for nixos-rebuild build-vm to work.
    home-manager = {
      useUserPackages = true;

      # I only need a subset of home-manager's capabilities. That is, access to its home.file, home.xdg.configFile and
      # home.xdg.dataFile so I can deploy files easily to my $HOME, but 'home-manager.users.gil0mendes.home.file.*' is
      # much too long and harder to maintain, so I've made aliases in:
      #
      #   home.file        ->  home-manager.users.gil0mendes.home.file
      #   home.configFile  ->  home-manager.users.gil0mendes.home.xdg.configFile
      #   home.dataFile    ->  home-manager.users.gil0mendes.home.xdg.dataFile
      #   home.programs    ->  home-manager.users.gil0mendes.home.programs
      #   home.services    ->  home-manager.users.gil0mendes.home.services
      users.${config.user.name} = {
        home = {
          file = mkAliasDefinitions options.home.file;
          # necessary for home-manager to work with flakes, otherwise it will look for a nixpkgs channel.
          stateVersion = config.system.stateVersion;
        };
        xdg = {
          enable = true;
          configFile = mkAliasDefinitions options.home.configFile;
          dataFile = mkAliasDefinitions options.home.dataFile;
        };

        programs = mkAliasDefinitions options.home.programs;
        services = mkAliasDefinitions options.home.services;
      };
    };

    users.users.${config.user.name} = mkAliasDefinitions options.user;

    # tell nix the trusted users
    nix = let users = [ "root" config.user.name ]; in {
      settings.trusted-users = users;
      settings.allowed-users = users;
    };

    # must already begin with pre-existing PATH. Also, can't use binDir here, because it contains a nix store path.
    env.PATH = [ "$DOTFILES_BIN" "$XDG_BIN_HOME" "$PATH" ];

    environment.extraInit =
      concatStringsSep "\n"
        (mapAttrsToList (n: v: "export ${n}=\"${v}\"") config.env);
  };
}

