{ config, options, lib, pkgs, my, ... }:

with lib;
with lib.my;
let
  devCfg = config.modules.dev;
  cfg = devCfg.java;
in
{
  options.modules.dev.java = {
    enable = mkBoolOpt false;
    xdg.enable = mkBoolOpt devCfg.xdg.enable;
  };

  config = mkMerge [
    (mkIf cfg.enable {
      user.packages = with pkgs; [
        maven
        sbt
        gradle
      ];

      # install JDK using home manager, this also sets the JAVA_HOME env var
      programs.java = {
        enable = true;
        package = pkgs.jdk21;
      };

      env.JDK_HOME = "${pkgs.jdk21}/lib/openjdk";
    })
  ];
}
