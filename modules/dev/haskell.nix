{ config, lib, pkgs, ... }:

with lib;
with lib.my;
let
  devCfg = config.modules.dev;
  configDir = config.dotfiles.configDir;
  cfg = devCfg.haskell;
in
{
  options.modules.dev.haskell = {
    enable = mkBoolOpt false;
    xdg.enable = mkBoolOpt devCfg.xdg.enable;
  };

  config = mkIf cfg.enable {
    home.file.".ghci".source = "${configDir}/ghci/.ghci";

    user.packages = with pkgs; [
      niv

      (haskellPackages.ghcWithHoogle (hpkgs:
        with hpkgs; [
          stack
          cabal-install
          cabal-fmt

          # Linter and formatters
          hlint
          # brittany FIXME broken
          ormolu
          fourmolu

          hasktags
          haskell-language-server
        ]))
    ];
  };
}
