{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.theme;
in {
  config = mkIf (cfg.active == "dark-momentum") (mkMerge [
    # Desktop-agnostic configuration
    {
      modules = {
        theme = {
          wallpaper = mkDefault ./config/wallpaper.jpg;
          gtk = {
            theme = "palenight";
            iconTheme = "Papirus-Dark";
            cursorTheme = "Numix-Cursor";
          };
          fonts = {
            sans.name = "Fira Sans";
            mono.name = "Fira Code";
          };
          colors = {
            black = "#1E2029";
            red = "#ffb86c";
            green = "#50fa7b";
            yellow = "#f0c674";
            blue = "#61bfff";
            magenta = "#bd93f9";
            cyan = "#8be9fd";
            silver = "#e2e2dc";
            grey = "#5B6268";
            brightred = "#de935f";
            brightgreen = "#0189cc";
            brightyellow = "#f9a03f";
            brightblue = "#8be9fd";
            brightmagenta = "#ff79c6";
            brightcyan = "#0189cc";
            white = "#f8f8f2";

            types.fg = "#bbc2cf";
            types.panelbg = "#21242b";
            types.border = "#1a1c25";
          };
        };

        shell.zsh.rcFiles = [ ./config/zsh/prompt.zsh ];
        desktop.browsers = {
          firefox = {
            userChrome = lib.readFile ./config/firefox/userChrome.css;
            userContent = lib.readFile ./config/firefox/userContent.css;
          };
        };
      };
    }

    # Desktop (X11) theming
    (mkIf config.services.xserver.enable {
      user.packages = with pkgs; [
        # theme
        palenight-theme
        # icons for rofi
        papirus-icon-theme
        # cursor
        numix-cursor-theme
      ];

      # Login screen theme
      # services.xserver.displayManager.lightdm.greeters.mini.extraConfig = ''
      #   text-color = "${cfg.colors.magenta}"
      #   password-background-color = "${cfg.colors.black}"
      #   window-color = "${cfg.colors.types.border}"
      #   border-color = "${cfg.colors.types.border}"
      # '';

      # Other dotfiles
      home.configFile = with config.modules; mkMerge [
        {
          # Sourced from sessionCommands in modules/themes/default.nix
          "xtheme/90-theme".source = ./config/Xresources;
        }
        (mkIf desktop.apps.rofi.enable {
          "rofi/theme" = { source = ./config/rofi; recursive = true; };
        })
      ];
    })
  ]);
}
