# Emacs is amazing and one day probably my main driver.

{ config, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.editors.emacs;
  configDir = config.dotfiles.configDir;
in
{
  options.modules.editors.emacs = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ inputs.emacs-overlay.overlay ];

    user.packages = with pkgs; [
      # packages required for emacs config works fully
      tree-sitter

      ((emacsPackagesFor emacsNativeComp).emacsWithPackages
        (epkgs: [ epkgs.vterm epkgs.jinx ]))
    ];

    fonts.packages = [ pkgs.emacs-all-the-icons-fonts ];
  };
}
