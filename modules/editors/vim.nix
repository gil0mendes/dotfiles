# Emacs and Vscode are awesome, but sometimes I just want to do something really quick on the terminar

{ config, lib, pkgs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.editors.vim;
in
{
  options.modules.editors.vim = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      editorconfig-core-c
      unstable.neovim
      asm-lsp
      stylua
      marksman
      eslint_d
    ];

    home.programs.neovim = {
      enable = true;
      withNodeJs = true;
      withPython3 = true;
    };

    environment.shellAliases = {
      vim = "nvim";
      v = "nvim";
    };
  };
}
