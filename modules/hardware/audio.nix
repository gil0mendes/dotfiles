{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.audio;
in {
  options.modules.hardware.audio = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    security.rtkit.enable = true;
    user.extraGroups = [ "audio" "pipewire" ];

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      jack.enable = true;
    };

    home.services.easyeffects.enable = true;
    user.packages = with pkgs; [
      easyeffects
      helvum
      pulsemixer
      pulseaudio
      pavucontrol
      pipewire.jack
    ];

    # configure bluetooth audio settings
    hardware.bluetooth.disabledPlugins = [ "sap" ];
    hardware.bluetooth.settings = {
      General = {
        MultiProfile = "multiple";
        FastConnectable = true;
        Privacy = "device";

        Enable = "Control,Gateway,Headset,Media,Sink,Socket,Source";
        Experimental = true;
        KernelExperimental = true;
      };

      GATT = { KeySize = 16; };

      AVDTP = {
        SessionMode = "ertm";
        StreamMode = "streaming";
      };
    };
  };
}
