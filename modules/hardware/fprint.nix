{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.fprint;
in {
  options.modules.hardware.fprint = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.fprintd.enable = true;
  };
}