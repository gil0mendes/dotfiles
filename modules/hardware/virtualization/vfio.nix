{ options, config, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.virtualization.vfio;
in {
  options.modules.hardware.virtualization.vfio = with types; {
    enable = mkBoolOpt false;
    IOMMUType = mkOption {
      type = enum [ "intel" "amd" ];
      example = "intel";
      description = "Type of the IOMMU used";
    };
    devices = mkOption {
      type = listOf (strMatching "[0-9a-f]{4}:[0-9a-f]{4}");
      default = [ ];
      example = [ "10de:1b80" "10de:10f0" ];
      description = "PCI IDs of devices to bind to vfio-pci";
    };
  };

  config = mkIf cfg.enable {
    services.udev.extraRules = ''
      SUBSYSTEM=="vfio", OWNER="root", GROUP="kvm"
    '';

    boot.kernelParams = (if cfg.IOMMUType == "intel" then [
      "intel_iommu=on"
    ] else [
      "amd_iommu=on"
    ]) ++ [ "iommu=pt" ] ++ (optional (builtins.length cfg.devices > 0)
      ("vfio-pci.ids=" + builtins.concatStringsSep "," cfg.devices));

    boot.kernelModules = [ "vfio_pci" "vfio_iommu_type1" "vfio" ];
    boot.initrd.kernelModules =
      [ "vfio_pci" "vfio_iommu_type1" "vfio" ];
  };
}
