{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.virtualization;
in {
  options.modules.hardware.virtualization = {
    enable = mkBoolOpt true;
  };

  config = mkIf cfg.enable {
    users.users."qemu-libvirtd" = {
      extraGroups = [ "kvm" "input" "libvirtd" ];
      isSystemUser = true;
    };

    # add user to libvirtd group
    user.extraGroups = [ "kvm" "libvirtd" "qemu-libvirtd" ];

    virtualisation = {
      libvirtd = {
        enable = true;
        onBoot = "ignore";
        onShutdown = "shutdown";

        qemu = {
          package = pkgs.qemu_kvm;
          ovmf = {
            enable = true;
          };

          # TPM support
          swtpm.enable = true;
          runAsRoot = true;
          verbatimConfig = ''
            cgroup_device_acl = [
              "/dev/vfio/vfio", "/dev/vfio/12", "/dev/kvm", "/dev/pts/ptmx", "/dev/shm/looking-glass", "/dev/null"
            ]

            namespaces = []
          '';
        };
      };

      # USB redirection in virtual machine
      spiceUSBRedirection.enable = true;
    };

    # KVM FrameRelay for Looking Glass
    modules.hardware.virtualization.kvmfr = {
      enable = true;
      shm = {
        enable = true;
        size = 128;
        user = config.user.name;
        group = "qemu-libvirtd";
        mode = "0666";
      };
    };

    programs.virt-manager.enable = true;

    # set hypervisor
    home-manager.users.${config.user.name}.dconf.settings = {
      "org/virt-manager/virt-manager/connections" = {
        autoconnect = [ "qemu:///system" ];
        uris = [ "qemu:///system" ];
      };
    };

    # add KVM FrameRelay client
    user.packages = with pkgs; [
      looking-glass-client
    ];

    # enable the capacity to launch vm with a virtual socket (network)
    boot.kernelModules = [ "vhost_vsock" ];
  };
}
