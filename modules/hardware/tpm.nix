{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.tpm;
in {
  options.modules.hardware.tpm = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    security.tpm2.enable = true;
    security.tpm2.pkcs11.enable = true;
    security.tpm2.tctiEnvironment.enable = true;

    user.extraGroups = [ "tss" ];
  };
}
