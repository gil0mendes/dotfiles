{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.fs;
in {
  options.modules.hardware.fs = {
    enable = mkBoolOpt false;
    ssd.enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable (mkMerge [
    {
      programs.udevil.enable = true;

      # support for more filesystems, to support external drivers
      environment.systemPackages = with pkgs; [
        sshfs
        exfat
        ntfs3g
      ];
    }

    (mkIf cfg.ssd.enable {
      services.fstrim.enable = true;
    })
  ]);
}