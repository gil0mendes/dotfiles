{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.hardware.gpu.intelxe;
in
{
  options.modules.hardware.gpu.intelxe = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    boot.kernelParams = [
      # Enable power-saving display C-states
      "i915.enable_dc=1"
      # Enable frame buffer compression for power savings
      "i915.enable_fbc=1"
      # Enable PSR
      "i915.enable_psr=1"
      # Display power wells are always on
      "i915.disable_power_well=0"
      # Enable GuC load for GuC submission and/or HuC load
      "i915.enable_guc=-1"
    ];

    environment.systemPackages = with pkgs; [ libva-utils ];
    hardware.opengl = {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver
        vaapiIntel
        vaapiVdpau
        libvdpau-va-gl
        intel-ocl
        intel-compute-runtime
      ];
    };

    # when the nvidia driver is disabled say to xserver to use intel GPU
    services.xserver.videoDrivers = lib.mkDefault [ "intel" ];

    environment.variables = {
      VDPAU_DRIVER = lib.mkDefault "va_gl";
    };
  };
}
