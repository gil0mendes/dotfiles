# Diagnostics
# Some diagnostic tools to help debug some hardward issues or bad performance

{ lib, config, options, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.system.diagnostics;
in {
  options.modules.system.diagnostics = {
    enable = mkBoolOpt false;
    benchmarks.enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable (mkMerge [
    {
      environment.systemPackages = with pkgs; [
        hwinfo
        ioping
        lm_sensors
        lsof
        pciutils # lspci
        powertop
        smartmontools
        sysstat # sar, iostat, pidsstat
        unixtools.netstat
        usbutils # lsusb
      ];
    }

    (mkIf cfg.benchmarks.enable {
      environment.systemPackages = with pkgs;	[
        sysbench
        unigine-valley
        speedtest-cli
      ];
    })
  ]);
}
