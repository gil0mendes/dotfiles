{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.desktop.xorg.gnome;
  configDir = config.dotfiles.configDir;
in
{
  options.modules.desktop.xorg.gnome = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.xserver = {
      enable = true;
      displayManager.gdm.enable = true;
      desktopManager.gnome.enable = true;
    };

    # remove some gnome tools that I don't need
    environment.gnome.excludePackages = (with pkgs; [
      gnome-tour
    ]) ++ (with pkgs.gnome; [
      gnome-music
      epiphany # web browser
      geary # email reader
      yelp # help view
      gnome-initial-setup
    ]);

    home-manager.users.${config.user.name}.dconf.settings = {
      "org/gnome/desktop/wm/preferences" = {
        workspace-names = [ "Main" ];
      };
      "org/gnome/desktop/interface" = {
        color-scheme = "prefer-dark";
        enable-hot-corners = false;
        text-scaling-factor = 1.1;
        font-antialiasing = "rgba";
      };
      "org/gnome/desktop/wm/keybindings" = {
        switch-to-workspace-left = [ "<Alt><Super>Left" ];
        switch-to-workspace-right = [ "<Alt><Super>Right" ];
      };
      "org/gnome/shell" = {
        disable-user-extensions = false;

        # `gnome-extensions list` for a list
        enabled-extensions = [
          "user-theme@gnome-shell-extensions.gcampax.github.com"
          "trayIconsReloaded@selfmade.pl"
          "Vitals@CoreCoding.com"
          "dash-to-panel@jderose9.github.com"
          "sound-output-device-chooser@kgshank.net"
          "space-bar@luchrioh"
        ];

        # configure dash to panel
        "extensions/dash-to-panel/panel-positions" = ''
          {"0":"TOP"}
        '';
        "extensions/dash-to-panel/dot-position" = "top";
      };
      "org/gnome/shell/extensions/vitals" = {
        show-storage = false;
        show-voltage = true;
        show-memory = true;
        show-fan = true;
        show-temperature = true;
        show-processor = true;
        show-network = true;
      };
    };

    user.packages = with pkgs; [
      ulauncher

      # gnome extensions
      gnomeExtensions.user-themes
      gnomeExtensions.tray-icons-reloaded
      gnomeExtensions.vitals
      gnomeExtensions.dash-to-panel
      gnomeExtensions.sound-output-device-chooser
      gnomeExtensions.space-bar
    ];

    environment.systemPackages = with pkgs;
      [ gnome.gnome-tweaks ];
    services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];

    services.power-profiles-daemon.enable = false;
    hardware.pulseaudio.enable = false;
  };
}
