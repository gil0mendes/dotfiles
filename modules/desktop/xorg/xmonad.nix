{ config, options, lib, pkgs, inputs, system, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.desktop.xorg.xmonad;
  configDir = config.dotfiles.configDir;
in
{
  options.modules.desktop.xorg.xmonad = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.xserver.windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      extraPackages = hpkgs:
        with hpkgs; [
          flow
        ];
      ghcArgs = [
        "-O3"
        # Compile with LLVM backend
        # "-fllvm -optlo-O3"
      ];
    };

    services.xserver.displayManager.defaultSession = "none+xmonad";

    environment.sessionVariables = {
      XMONAD_CONFIG_DIR = "$XDG_CONFIG_HOME/xmonad";
      XMONAD_CACHE_DIR = "$XDG_CACHE_HOME/xmonad";
      XMONAD_DATA_DIR = "$XDG_DATA_HOME/xmonad";
    };

    user.packages = with pkgs; [
      haskellPackages.xmobar
      haskellPackages.xmonad-extras
      trayer # tray
      playerctl # current track script
      pstree # window swallowing
      xdotool # clicable workspaces
      autorandr # to use with rescreen
      xkb-switch # switch kbd layout when needed
    ];

    fonts.packages = with pkgs; [
      siji # some nice icons (awfull on hidpi)
      font-awesome # even more nice icons
      weather-icons # for weather script
    ];

    env.PATH = [ "$XMONAD_CONFIG_DIR/scripts/xmobar" ];

    # link configuration
    # home.configFile = {
    #   "xmonad" = {
    #     source = "${configDir}/xmonad";
    #     recursive = true;
    #   };
    # };
  };
}
