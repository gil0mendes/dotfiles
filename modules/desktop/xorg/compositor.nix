{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.desktop.xorg.compositor;
  configDir = config.dotfiles.configDir;
in
{
  options.modules.desktop.xorg.compositor = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.picom = {
      backend = "glx";
      vSync = true;

      activeOpacity = 1.0;
      inactiveOpacity = 0.92;
      opacityRules = [
        "100:name *= 'i3lock'"
        "100:class_g = 'Gimp'"
        "100:class_g = 'Inkspace'"
        "100:class_g = 'feh'"
        "100:class_g = 'Thunderbird'"
        "100:class_g = 'VirtualBox Machine'"
        "100:class_g = 'mpv'"
        "100:class_g = 'Rofi'"
        # "0:_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
        # "96:_NET_WM_STATE@:32a *= '_NET_WM_STATE_STICKY'"
      ];

      shadow = true;
      shadowOffsets = [ (-3) (-3) ];
      shadowOpacity = 0.6;
      shadowExclude = [
        "name *= 'picom'"
        "class_g = 'trayer'"
        "class_g ?= 'Notify-osd'"
        "class_g = 'Ulauncher'"
        "_GTK_FRAME_EXTENTS@:c"
        "_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
      ];

      settings = {
        inactive-dim = 0.3;
        shadow-radius = 8;

        wintypes = {
          normal = {
            fade = false;
            shadow = true;
          };
          tooltip = {
            fade = true;
            shadow = true;
            opacity = 0.75;
            focus = true;
            full-shadow = false;
          };
          # dock = { shadow = false; }
          dnd = { shadow = true; };
          popup_menu = { opacity = 1.0; };
          dropdown_menu = { opacity = 1.0; };
        };

        focus-exclude = [
          "class_g = 'xob'"
          "class_g = 'trayer'"
          "class_g *?= 'safeeyes'"
          "class_g *?= 'skippy'"
        ];

        use-damage = true;

        # Unredirect all windows if a full-screen opaque window is detected, to
        # maximize performance for full-screen windows. Known to cause
        # flickering when redirecting/unredirecting windows.
        unredir-if-possible = true;

        # Use X Sync fence to sync clients' draw calls, to make sure all draw
        # calls are finished before picom starts drawing. Needed on
        # nvidia-drivers with GLX backend for some users.
        xrender-sync-fence = true;

        # Other
        mark-wmwin-focused = false;
        mark-ovredir-focused = false;
        use-ewmh-active-win = true;
        detect-rounded-corners = true;
        detect-client-opacity = true;
        detect-transient = true;
        detect-client-leader = true;
      };
    };
  };
}
