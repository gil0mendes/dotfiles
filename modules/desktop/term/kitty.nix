{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.term.kitty;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.term.kitty = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    # xst-256color isn't supported over ssh, so revert to a known one
    modules.shell.zsh.rcInit = ''
      [ "$TERM" = xst-256color ] && export TERM=xterm-256color
    '';

    user.packages = with pkgs; [
      kitty
      (makeDesktopItem {
        name = "kitty";
        desktopName = "Kitty";
        genericName = "Default terminal";
        icon = "utilities-terminal";
        exec = "${kitty}/bin/kitty";
        categories = [ "Development" "System" "Utility" ];
      })
    ];

    home.configFile = {
      "kitty" = { source = "${configDir}/kitty"; recursive = true; };
    };
  };
}
