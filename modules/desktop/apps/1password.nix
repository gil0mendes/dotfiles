# TODO: find a way to start the SSH agent with the system

{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.apps.onePassword;
in {
  options.modules.desktop.apps.onePassword = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    programs._1password.enable = true;
    programs._1password-gui = {
      enable = true;


      # Allow CLI integration
      polkitPolicyOwners = [ config.user.name ];
    };

    # Install a system keychain, 1Password requires this in order to store the 
    # two-factor authentication token.
    services.gnome.gnome-keyring.enable = true;
    security.pam.services.lightdm.enableGnomeKeyring = true;
  };
}
