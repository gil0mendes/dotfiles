{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop;
in {
  config = mkIf config.services.xserver.enable {
    assertions = [
      {
        assertion = (countAttrs (n: v: n == "enable" && value) cfg) < 2;
        message = "Can't have more than one desktop environment enabled at a time";
      }
      {
        assertion =
          let srv = config.services;
          in srv.xserver.enable ||
            srv.sway.enable ||
            !(anyAttrs
              (n: v: isAttrs v &&
                anyAttrs (n: v: isAttrs v && v.enable))
              cfg);
        message = "Can't enable a desktop app without a desktop environment";
      }
    ];

    services.xserver = {
      autoRepeatDelay = 200;
      autoRepeatInterval = 50;
      xkb.layout = "us,pt";
    };

    services.dbus.packages = with pkgs; [ dconf ];
    programs.dconf.enable = true;

    user.packages = with pkgs; [
      xclip
      xdotool
      xorg.xwininfo
      xorg.libX11

      # add modern and fast spell-checker along side some dictionaries
      enchant # generic spell checking library
      nuspell
      hunspellDicts.en_US
      hunspellDicts.pt_PT
    ];

    fonts = {
      fontDir.enable = true;
      enableGhostscriptFonts = true;
      packages = with pkgs; [
        ubuntu_font_family
        dejavu_fonts
        meslo-lgs-nf
        jetbrains-mono
      ];
    };

    # Try really hard to get QT to respect my GTK theme.
    env.GTK_DATA_PREFIX = [ "${config.system.path}" ];
    env.QT_QPA_PLATFORMTHEME = "gnome";
    env.QT_STYLE_OVERRIDE = "kvantum";

    # Fix issue with Eletron apps with Wayland
    env.NIXOS_OZONE_WL = "1";

    services.xserver.displayManager.sessionCommands = ''
      # GTK2_RC_FILES must be available to the display manager.
      export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
    '';

    # Clean up leftovers, as much as we can
    system.userActivationScripts.cleanupHome = ''
      pushd "${config.user.home}"
      rm -rf .compose-cache .nv .pki .dbus .fehbg
      [ -s .xsession-errors ] || rm -f .xsession-errors*
      popd
    '';
  };
}
