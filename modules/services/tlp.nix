{ options, config, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.tlp;
in {
  options.modules.services.tlp = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.tlp.enable = true;
    services.tlp.settings = {
      # Set CPU governors
      CPU_SCALING_GOVERNOR_ON_AC = "schedutil";
      CPU_SCALING_GOVERNOR_ON_BAT = "schedutil";

      # Disable Turbo [Boost|Core] on battery
      CPU_BOOST_ON_AC = 1;
      CPU_BOOST_ON_BAT = 0;

      # Intel CPU HWP hints
      CPU_ENERGY_PERF_POLICY_ON_AC = "balance_performance";
      CPU_ENERGY_PERF_POLICY_ON_BAT = "balance_power";

      # Set CPU Frequency constraints
      CPU_MIN_PERF_ON_AC = 0;
      CPU_MAX_PERF_ON_AC = 100;
      CPU_MIN_PERF_ON_BAT = 0;
      CPU_MAX_PERF_ON_BAT = 40;

      # Performance profile for the platform
      PLATFORM_PROFILE_ON_AC = "performance";
      PLATFORM_PROFILE_ON_BAT = "low-power";

      # TODO: Check tpl-stat -s for MEM_SLEEP

      # USB power controll
      AHCI_RUNTIME_PM_ON_AC = "auto";
      AHCI_RUNTIME_PM_ON_BAT = "auto";

      # PCI ASPM profile
      PCIE_ASPM_ON_AC = "performance";
      PCIE_ASPM_ON_BAT = "powersupersave";

      # power down PCI devices whem idle
      RUNTIME_PM_ON_AC = "auto";
      RUNTIME_PM_ON_BAT = "auto";

      # disable power management for nvidia
      RUNTIME_PM_DRIVER_DENYLIST = "nouveau nvidia";
    };

    environment.systemPackages = [ config.boot.kernelPackages.x86_energy_perf_policy ];
    nixpkgs.overlays = [
      (
        _final: prev: {
          tlp = prev.tlp.override {
            inherit (config.boot.kernelPackages) x86_energy_perf_policy;
          };
        }
      )
    ];
  };
}
