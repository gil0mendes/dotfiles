# Documentation for the flatpaks flake: https://github.com/GermanBread/declarative-flatpak/blob/dev/docs/definition.md

{ options, config, lib, inputs, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.flatpak;
in {
  options.modules.services.flatpak = {
    enable = mkBoolOpt false;
  };

  imports = with inputs; [
    flatpaks.nixosModules.default
  ];

  config = mkIf cfg.enable {
    services.flatpak = {
      enable = true;
      # packages follow this format: `remote-name:type/package-name/arch/branch-name:commit`
      packages = [
        # emoji support
        "flathub:app/com.tomjwatson.Emote//stable"
      ];
    };
  };
}
