# Keyboard remapper
{ config, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.kanata;
in {
  options.modules.services.kanata = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.kanata = {
      enable = true;
      keyboards = {
        default = {
          devices = [
            "/dev/input/by-path/platform-i8042-serio-0-event-kbd"
          ];
          extraDefCfg = "process-unmapped-keys yes";
          config = ''
                        (defsrc 
                         caps
                        )
                        (deflayer base
                         @cap
                        )
                        (defalias
            							cap (tap-hold-release 1 130 esc lctl)
                        )
          '';
        };
      };
    };
  };
}
