# NVIM Config

## After install

After install these configurations there are some commands to run in order to complete the setup:

```text
# this will connect with the waketime service
:WakaTimeApiKey

# this command will authenticate copilot
:Copilot auth
```

## Keybindinds
- https://github.com/numToStr/Comment.nvim
- https://github.com/tpope/vim-surround
