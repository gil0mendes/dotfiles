local core = require("util.core")
local format = require("util.format")

return {
	{
		"stevearc/conform.nvim",
		dependencies = { "mason.nvim" },
		lazy = true,
		cmd = { "ConformInfo" },
		keys = {
			{
				"<leader>cF",
				function()
					require("conform").format({ formatters = { "injected" }, timeout_ms = 3000 })
				end,
				mode = { "n", "v" },
				desc = "Format Injected Langs",
			},
		},
		init = function()
			-- Install the conform formatter on VeryLazy
			core.on_very_lazy(function()
				format.register({
					name = "conform.nvim",
					priority = 100,
					primary = true,
					format = function(buf)
						require("conform").format({ bufnr = buf })
					end,
					sources = function(buf)
						local ret = require("conform").list_formatters(buf)

						return vim.tbl_map(function(v)
							return v.name
						end, ret)
					end,
				})
			end)
		end,
		opts = function()
			local opts = {
				default_format_opts = {
					timeout_ms = 3000,
					async = false,
					quiet = false,
					lsp_format = "fallback",
				},
				formatters_by_ft = {
					asm = { "asmfmt" },
					c = { "clang-format" },
					css = { "prettier" },
					fish = { "fish_indent" },
					html = { "prettier" },
					lua = { "stylua" },
					markdown = { "prettier" },
					nix = { "nixpkgs_fmt" },
					python = { "isort", "black" },
					sh = { "shfmt" },
				},
				formatters = {
					injected = { options = { ignore_errors = true } },
				},
			}

			return opts
		end,
		config = function(_, opts)
			local conform = require("conform")

			conform.setup(opts)
		end,
	},
}
