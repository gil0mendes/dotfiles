if true then
	return {}
end

return {
	{
		"nvim-telescope/telescope.nvim",
		branch = "0.1.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
			{
				"nvim-telescope/telescope-fzf-native.nvim",
				build = "make",
				cond = vim.fn.executable("cmake") == 1,
			},
			{
				"nvim-telescope/telescope-live-grep-args.nvim",
				version = "v1.1.0",
			},
		},
		keys = {
			-- fast access
			{
				"<leader>,",
				":Telescope buffers sort_mru=true sort_lastused=true<CR>",
				desc = "Switch Buffer",
			},
			{
				"<leader>/",
				function()
					require("telescope.builtin").live_grep()
				end,
				desc = "Grep (Root Dir)",
			},
			{
				"<leader>:",
				":Telescope command_history<CR>",
				desc = "Command History",
			},
			{
				"<leader><space>",
				function()
					require("telescope.builtin").find_files({ hidden = true })
				end,
				desc = "Find Files (Root Dir)",
			},

			-- find
			{ "<leader>fb", ":Telescope buffers sort_mru=true sort_lastused=true<CR>", desc = "Buffers" },
			{
				"<leader>ff",
				function()
					require("telescope.builtin").files({ hidden = true })
				end,
				desc = "Find Files (Root Dir)",
			},
			{ "<leader>fg", ":Telescope git_files<CR>", desc = "Find Files (git-files)" },
			{ "<leader>fr", ":Telescope oldfiles<CR>", desc = "Recent" },

			-- search
			{ "<leader>sb", ":Telescope current_buffer_fuzzy_find<CR>", desc = "Buffer" },
			{ "<leader>sd", ":Telescope diagnostics bufnr=0<CR>", desc = "Buffer Diagnostics" },
			{ "<leader>sD", ":Telescope diagnostics<CR>", desc = "Workspace Diagnostics" },
			{
				"<leader>sg",
				function()
					require("telescope.builtin").live_grep()
				end,
				desc = "Grep (Root Dir)",
			},
			{
				"<leader>sG",
				function()
					require("telescope").extensions.live_grep_args.live_grep_args()
				end,
				desc = "Grep (With Args)",
			},
			{ "<leader>sh", ":Telescope help_tags<CR>", desc = "Help Pages" },
			{ "<leader>sH", ":Telescope highlights<CR>", desc = "Search Hightlights Groups" },
			{ "<leader>sk", ":Telescope keymaps<CR>", desc = "Key Maps" },
			{ "<leader>sR", ":Telescope resume<CR>", desc = "Resume" },
			{
				"<leader>sw",
				function()
					require("telescope.builtin").grep_string({ word_match = "-w" })
				end,
				desc = "Word (Root Dir)",
			},
			{
				"<leader>sW",
				function()
					require("telescope.builtin").grep_string({ root = false, word_match = "-w" })
				end,
				desc = "Word (cwd)",
			},
			{
				"<leader>sw",
				function()
					require("telescope.builtin").grep_string()
				end,
				mode = "v",
				desc = "Selection (Root Dir)",
			},
			{
				"<leader>sW",
				function()
					require("telescope.builtin").grep_string({ root = false })
				end,
				mode = "v",
				desc = "Word (cwd)",
			},
		},
		opts = function()
			local actions = require("telescope.actions")
			local telescope = require("telescope")
			local telescope_lga = require("telescope-live-grep-args.actions")

			-- Load live grep args for better search
			telescope.load_extension("live_grep_args")

			-- Enable telescope fzf native, if installed
			pcall(telescope.load_extension, "fzf")
			return {

				defaults = {
					prompt_prefix = " ",
					selection_caret = " ",
					mappings = {
						i = {
							["<C-k>"] = actions.move_selection_previous,
							["<C-j>"] = actions.move_selection_next,
							["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
							["<C-x>"] = actions.delete_buffer,
							["<C-s>"] = telescope_lga.quote_prompt(),
						},
						n = {
							["q"] = actions.close,
						},
					},
					file_ignore_patterns = {
						"node_modules",
						"yarn.lock",
						".git",
					},
					hidden = true,
				},
				pickers = {
					find_files = {
						find_command = { "rg", "--files", "--color", "never", "-g", "!.git" },
						hidden = true,
					},
				},
			}
		end,
	},

	-- improve ui with telescope
	{
		"stevearc/dressing.nvim",
		lazy = true,
		init = function()
			---@diagnostic disable-next-line: duplicate-set-field
			vim.ui.select = function(...)
				require("lazy").load({ plugins = { "dressing.nvim" } })
				return vim.ui.select(...)
			end

			---@diagnostic disable-next-line: duplicate-set-field
			vim.ui.input = function(...)
				require("lazy").load({ plugins = { "dressing.nvim" } })
				return vim.ui.input(...)
			end
		end,
	},
}
