return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			---@type lspconfig.options
			servers = {
				eslint = {
					settings = {
						-- allow eslint to search for the eslintrc when we are in a subfolder
						workingDirectories = { mode = "auto" },
						format = true,
					},
				},
			},
		},
		setup = {
			eslint = function()
				local formatter = LazyVim.lsp.formatter({
					name = "eslint: lsp",
					primary = false,
					priority = 200,
					filter = "eslint",
				})

				-- register the formatter with LazyVim
				LazyVim.format.register(formatter)
			end,
		},
	},
}
