return {
	-- syntax
	{
		"nvim-treesitter/nvim-treesitter",
		opts = { ensure_installed = { "terraform", "hcl" } },
	},

	-- LSP
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				terraformls = {},
			},
		},
	},
	-- ensure terraform tools are installed
	{
		"williamboman/mason.nvim",
		opts = { ensure_installed = { "tflint" } },
	},
	-- lint
	{
		"mfussenegger/nvim-lint",
		opts = {
			linters_by_ft = {
				terraform = { "terraform_validate" },
				tf = { "terraform_validate" },
			},
		},
	},
	-- format
	{
		"stevearc/conform.nvim",
		optional = true,
		opts = {
			formatters_by_ft = {
				hcl = { "packer_fmt" },
				terraform = { "terraform_fmt" },
				tf = { "terraform_fmt" },
				["terraform-vars"] = { "terraform_fmt" },
			},
		},
	},
}
