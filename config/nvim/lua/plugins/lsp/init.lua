local format = require("util.format")
local lsp = require("util.lsp")

return {
	-- lspconfig
	{
		"neovim/nvim-lspconfig",
		event = "LazyFile",
		-- cmd = { "LspInfo", "LspInstall", "LspUninstall", "Mason" },
		dependencies = {
			-- Plugin and UI to automatically install LSPs to stdpath
			"mason.nvim",
			{ "williamboman/mason-lspconfig.nvim", config = function() end },
		},
		opts = function()
			local globals = require("config.globals")

			---@class PluginLspOpts
			local ret = {
				---@type vim.diagnostic.Opts
				diagnostics = {
					underline = true,
					update_in_insert = false,
					virtual_text = {
						spacing = 4,
						source = "if_many",
						prefix = "●",
					},
					severity_sort = true,
					signs = {
						text = {
							[vim.diagnostic.severity.ERROR] = globals.config.icons.diagnostics.Error,
							[vim.diagnostic.severity.WARN] = globals.config.icons.diagnostics.Warn,
							[vim.diagnostic.severity.HINT] = globals.config.icons.diagnostics.Hint,
							[vim.diagnostic.severity.INFO] = globals.config.icons.diagnostics.Info,
						},
					},
				},
				inlay_hints = {
					enabled = true,
				},
				codelens = {
					enabled = false,
				},
				-- add any global capabilities here
				capabilities = {
					workspace = {
						fileOperations = {
							didRename = true,
							willRename = true,
						},
					},
				},
				-- options for vim.lsp.buf.format, `bufnr` and `filter` is handled
				-- by our formatter, but can be also overridden when specified
				format = {
					formatting_options = nil,
					timeout_ms = nil,
				},
				---LSP server settings
				---@type lspconfig.options
				servers = {
					asm_lsp = {},
					bashls = {},
					clangd = {
						cmd = { "clangd", "--offset-encoding=utf-16" },
					},
					cssls = {},
					html = {},
					jsonls = {},
					lua_ls = {
						settings = {
							Lua = {
								runtime = { version = "LuaJIT" },
								workspace = {
									checkThirdParty = false,
									library = {
										unpack(vim.api.nvim_get_runtime_file("", true)),
									},
								},
								telemetry = { enabled = false },
								codeLens = {
									enable = true,
								},
								completion = {
									callSnippet = "Replace",
								},
								doc = {
									privateName = { "^_" },
								},
								hint = {
									enable = true,
									setType = false,
									paramType = true,
									paramName = "Disable",
									semicolon = "Disable",
									arrayIndex = "Disable",
								},
							},
						},
					},
					marksman = {},
					nil_ls = {},
					pyright = {},
					yamlls = {},
					rust_analyzer = {
						check = {
							command = "clippy",
						},
					},
				},
				setup = {},
			}

			return ret
		end,
		config = function(_, opts)
			local globals = require("config.globals")

			-- setup autoformat
			format.register(lsp.formatter())

			-- setup keymaps
			lsp.on_attach(function(client, buffer)
				require("plugins.lsp.keymaps").on_attach(client, buffer)
			end)

			lsp.setup()
			lsp.on_dynamic_capability(require("plugins.lsp.keymaps").on_attach)

			-- diagnostics signs
			for severity, icon in pairs(opts.diagnostics.signs.text) do
				local name = vim.diagnostic.severity[severity]:lower():gsub("^%l", string.upper)
				name = "DiagnosticSign" .. name
				vim.fn.sign_define(name, { text = icon, texthl = name, numhl = "" })
			end

			-- inlay hints and code lens
			--TODO: make this dynamic, I still don't know if I want this

			-- Configure virtual text
			opts.diagnostics.virtual_text.prefix = function(diagnostic)
				local icons = globals.config.icons.diagnostics
				for d, icon in pairs(icons) do
					if diagnostic.severity == vim.diagnostic.severity[d:upper()] then
						return icon
					end
				end
			end

			-- Code lens
			---@diagnostic disable-next-line: unused-local
			lsp.on_supports_method("textDocument/codeLens", function(client, buffer)
				vim.lsp.codelens.refresh()
				vim.api.nvim_create_autocmd({ "BufEnter", "CursorHold", "InsertLeave" }, {
					buffer = buffer,
					callback = vim.lsp.codelens.refresh,
				})
			end)

			vim.diagnostic.config(vim.deepcopy(opts.diagnostics))

			-- LSP servers to install
			local servers = opts.servers
			local capabilities = vim.tbl_deep_extend(
				"force",
				{},
				vim.lsp.protocol.make_client_capabilities(),
				require("blink.cmp").get_lsp_capabilities(),
				opts.capabilities or {}
			)

			---Function to setup the server
			local function setup(server)
				local server_opts = vim.tbl_deep_extend("force", {
					capabilities = vim.deepcopy(capabilities),
				}, servers[server] or {})
				if server_opts.enabled == false then
					return
				end

				-- when there is a setup defined for the server run it
				if opts.setup[server] then
					if opts.setup[server](server, server_opts) then
						return
					end
				elseif opts.setup["*"] then
					if opts.setup["*"](server, server_opts) then
						return
					end
				end

				require("lspconfig")[server].setup(server_opts)
			end

			-- get all the servers that are available through mason-lspconfig
			local have_mason, mlsp = pcall(require, "mason-lspconfig")
			local all_mslp_servers = {}
			if have_mason then
				all_mslp_servers = vim.tbl_keys(require("mason-lspconfig.mappings.server").lspconfig_to_package)
			end

			local ensure_installed = {} ---@type string[]
			for server, server_opts in pairs(servers) do
				if server_opts then
					server_opts = server_opts == true and {} or server_opts
					if server_opts.enabled ~= false then
						-- run manual setup if mason=false or if this is a server that cannot be installed with mason-lspconfig
						if server_opts.mason == false or not vim.tbl_contains(all_mslp_servers, server) then
							setup(server)
						else
							ensure_installed[#ensure_installed + 1] = server
						end
					end
				end
			end

			if have_mason then
				mlsp.setup({
					ensure_installed = vim.tbl_deep_extend(
						"force",
						ensure_installed,
						LazyVim.plugin_opts("mason-lspconfig.nvim").ensure_installed or {}
					),
					handlers = { setup },
				})
			end

			-- avoid using denols and vtsls at the same time
			if LazyVim.lsp.is_enabled("denols") and LazyVim.lsp.is_enabled("vtsls") then
				local is_deno = require("lspconfig.util").root_pattern("deno.json", "deno.jsonc")
				LazyVim.lsp.disable("vtsls", is_deno)
				LazyVim.lsp.disable("denols", function(root_dir, config)
					if not is_deno(root_dir) then
						config.settings.deno.enable = false
					end
					return false
				end)
			end
		end,
	},

	-- cmdline tools and lsp servers
	{

		"williamboman/mason.nvim",
		cmd = "Mason",
		keys = { { "<leader>cm", "<cmd>Mason<cr>", desc = "Mason" } },
		build = ":MasonUpdate",
		opts_extend = { "ensure_installed" },
		opts = {
			ensure_installed = {
				"stylua",
				"shfmt",
			},
		},
		---@param opts MasonSettings | {ensure_installed: string[]}
		config = function(_, opts)
			require("mason").setup(opts)
			local mr = require("mason-registry")
			mr:on("package:install:success", function(pkg)
				vim.defer_fn(function()
					-- trigger FileType event to possibly load this newly installed LSP server
					require("lazy.core.handler.event").trigger({
						event = "FileType",
						buf = vim.api.nvim_get_current_buf(),
					})
				end, 100)

				-- only execute this on Linux, because is where I use NixOS
				if vim.uv.os_uname().sysname == "Linux" then
					pkg:get_receipt():if_present(function(receipt)
						for _, rel_path in pairs(receipt.links.bin) do
							local bin_abs_path = pkg:get_install_path() .. "/libexec/bin/" .. rel_path
							os.execute(
								'patchelf --set-interpreter "$(patchelf --print-interpreter $(grep -oE \\/nix\\/store\\/[a-z0-9]+-neovim-unwrapped-[0-9]+\\.[0-9]+\\.[0-9]+\\/bin\\/nvim $(which nvim)))" '
									.. bin_abs_path
							)
						end
					end)

					pkg:get_receipt():if_present(function(receipt)
						for _, rel_path in pairs(receipt.links.bin) do
							local bin_abs_path = pkg:get_install_path() .. "/libexec/bin/" .. rel_path
							os.execute(
								'patchelf --set-interpreter "$(patchelf --print-interpreter $(grep -oE \\/nix\\/store\\/[a-z0-9]+-neovim-unwrapped-[0-9]+\\.[0-9]+\\.[0-9]+\\/bin\\/nvim $(which nvim)))" '
									.. bin_abs_path
							)
						end
					end)
				end
			end)

			mr.refresh(function()
				for _, tool in ipairs(opts.ensure_installed) do
					local p = mr.get_package(tool)
					if not p:is_installed() then
						p:install()
					end
				end
			end)
		end,
	},
}
