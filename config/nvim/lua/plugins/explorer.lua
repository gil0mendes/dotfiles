return {
	{
		"folke/snacks.nvim",
		opts = {
			explorer = {},
		},
		keys = {
			{
				"<leader>fe",
				function()
					Snacks.explorer({ cwd = LazyVim.root() })
				end,
				desc = "Explorer (root dir)",
			},
			{
				"<leader>fE",
				function()
					Snacks.explorer()
				end,
				desc = "Explorer (cwd)",
			},
		},
	},

	{
		"stevearc/oil.nvim",
		---@module 'oil'
		---@type oil.SetupOpts
		opts = {},
		dependencies = {
			{ "echasnovski/mini.icons", opts = {} },
		},
		lazy = false,
		config = function()
			local oil = require("oil")

			local function start_search(opts)
				opts = opts or {}
				local dir = oil.get_current_dir()
				if not dir then
					return
				end

				-- close oil before open picker otherwise it will open inside the oil buffer
				oil.close()

				Snacks.picker.grep({ cwd = dir })
			end

			oil.setup({
				keymaps = {
					["g?"] = "actions.show_help",
					["<CR>"] = "actions.select",
					["<C-\\>"] = "actions.select_vsplit",
					["<C-enter>"] = "actions.select_split",
					["<C-p>"] = "actions.preview",
					["<C-c>"] = "actions.close",
					["<ESC>"] = "actions.close",
					["<C-r>"] = "actions.refresh",
					["-"] = "actions.parent",
					["_"] = "actions.open_cwd",
					["`"] = "actions.cd",
					["~"] = "actions.tcd",
					["gs"] = "actions.change_sort",
					["gx"] = "actions.open_external",
					["g."] = "actions.toggle_hidden",
					["<C-s>"] = { desc = "Search string in this directory", callback = start_search },
				},
				use_default_keymaps = false,
				default_file_explorer = true,
				delete_to_trash = true,
				skip_confirm_for_simple_edits = true,
				view_options = {
					show_hidden = true,
					natural_order = true,
					is_always_hidden = function(name, _)
						return name == ".." or name == ".git"
					end,
				},
				win_options = {
					wrap = true,
				},
			})
		end,
		keys = {
			{
				"<leader>e",
				function()
					require("oil").toggle_float()
				end,
				desc = "Oil",
			},
		},
	},
}
