require("user.options")
require("user.lazy")
require("user.keymaps")
require("user.highlight_yank")

local format = require("util.format")

format.setup()
