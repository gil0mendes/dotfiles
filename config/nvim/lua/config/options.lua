-- this file is automatically loaded by util.core

-- Set leader key to space
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Enable editor config
vim.g.editorconfig = true

-- enable auto format
vim.g.autoformat = true

-- Snacks animations
-- Set to `false` to globally disable all snacks animations
vim.g.snacks_animate = true

-- Root dir detection
-- Each entry can be:
-- * the name of a detector function like `lsp` or `cwd`
-- * a pattern or array of patterns like `.git` or `lua`.
-- * a function with signature `function(buf) -> string|string[]`
vim.g.root_spec = { "lsp", { ".git", "lua" }, "cwd" }

local opt = vim.opt

-- Enable relative line numbers
opt.nu = true
opt.rnu = true

-- Set tabs to 2 spaces
opt.tabstop = 2
opt.softtabstop = 2

-- add fill characters
opt.fillchars = {
	foldopen = "",
	foldclose = "",
	fold = " ",
	foldsep = " ",
	diff = "╱",
	eob = " ",
}

-- show some invisible characters
opt.list = true

-- Hide * markup for bold and italic, but not markers with substitutions
opt.conceallevel = 2

-- Enable auto indenting
opt.smartindent = true
opt.shiftwidth = 2

-- Enable smart indenting
opt.breakindent = true

-- Enable incremental searching
opt.incsearch = true
opt.hlsearch = true

-- Disable text wrap
opt.wrap = false

-- Better splitting
opt.splitbelow = true
opt.splitright = true

-- confirm to save changes before exiting modified buffer
opt.confirm = true

-- Enable mouse mode
opt.mouse = "a"

-- configure some message behaviour
opt.shortmess:append({ W = true, I = true, c = true, C = true })

-- Enable ignorecase + smartcase for better searching
opt.ignorecase = true
opt.smartcase = true

-- Set completeopt to have a better completion experience
opt.completeopt = "menu,menuone,noselect"

-- Enable persistent undo history
opt.undofile = true

-- Enable 24-bit colors
opt.termguicolors = true

-- Always show the signcolumn, otherwise it would shift the text each time
opt.signcolumn = "yes"

-- Enable access to system clipboard when not inside a ssh session
opt.clipboard = vim.env.SSH_TTY and "" or "unnamedplus"

opt.sessionoptions = { "buffers", "curdir", "tabpages", "winsize", "help", "globals", "skiprtp", "folds" }

-- Enable cursor line highlight
opt.cursorline = true

-- Set fold settings
-- This options are recommended by nvim-ufo
opt.foldcolumn = "0"
opt.foldlevel = 99
opt.foldlevelstart = 99
opt.foldenable = true
opt.formatexpr = "v:lua.require'util'.format.formatexpr()"
opt.formatoptions = "jcroqlnt"
opt.foldmethod = "expr"
opt.foldtext = ""

-- Always keep 8 lines above/below cursor unless at start/end of file
opt.scrolloff = 8

-- Place a column line
opt.colorcolumn = "120"

-- Configure spell checking (most of what I right with VIM is in English)
opt.spelllang = "en_us"
opt.spell = true

-- configure grep opts
opt.grepformat = "%f:%l:%c:%m"
opt.grepprg = "rg --vimgrep"

-- make the scroll smooth
opt.smoothscroll = true

-- use snacks to render the status column
opt.statuscolumn = [[%!v:lua.require'snacks.statuscolumn'.get()]]
