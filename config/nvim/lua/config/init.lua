local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.uv.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

local core = require("util.core")
_G.LazyVim = core
local format = require("util.format")

local lazy_clickboard

---@class LazyVimConfig: LazyVimOptions
local M = {}

-- make config global available
LazyVim.config = M

M.lazy_file_events = { "BufReadPost", "BufNewFile", "BufWritePre" }

function M.initLazy()
	-- add support for the LazyFile event
	local Event = require("lazy.core.handler.event")

	Event.mappings.LazyFile = { id = "LazyFile", event = M.lazy_file_events }
	Event.mappings["User LazyFile"] = Event.mappings.LazyFile

	-- load lazy.nvim
	require("lazy").setup({
		spec = {
			{ import = "plugins" },
			-- langs
			{ import = "plugins.lang" },
			-- format
			{ import = "plugins.format" },
		},
		defaults = { lazy = false },
		checker = {
			enabled = true,
		},
		rocks = { hererocks = true },
		performance = {
			cache = {
				enabled = true,
			},
			rtp = {
				disabled_plugins = {
					"gzip",
					"rplugin",
					"tarPlugin",
					"tohtml",
					"tutor",
					"zipPlugin",
				},
			},
		},
	})
end

function M.load(name)
	local function _load(mod)
		if require("lazy.core.cache").find(mod)[1] then
			require(mod)
		end
	end

	_load("config." .. name)
end

function M.setup()
	-- the options need to be loaded before lazy.nvim
	M.load("options")

	-- defer built-in clipboard
	lazy_clickboard = vim.opt.clipboard
	vim.opt.clipboard = ""

	-- initilize Lazy.nvim
	M.initLazy()

	-- autocmds can be loaded lazily when not opening a file
	local lazy_autocmds = vim.fn.argc(-1) == 0
	if not lazy_autocmds then
		M.load("autocmds")
	end

	local group = vim.api.nvim_create_augroup("LazyVim", { clear = true })
	vim.api.nvim_create_autocmd("User", {
		group = group,
		pattern = "VeryLazy",
		callback = function()
			if lazy_autocmds then
				M.load("autocmds")
			end

			-- load my keymaps
			M.load("keymaps")

			-- restore the clipboard functionality
			if lazy_clickboard ~= nil then
				vim.opt.clipboard = lazy_clickboard
			end

			format.setup()
		end,
	})

	-- core.track("colorsheme")
	-- core.try(function()
	-- 	vim.cmd.colorsheme("rose-pine")
	-- end, {
	-- 	msg = "Could not load your colorsheme",
	-- 	on_error = function(msg)
	-- 		core.error(msg)
	-- 		vim.cmd.colorsheme("habamax")
	-- 	end,
	-- })
	-- core.track()
end

return M
