-- enable the neovim experimental loader module
if vim.loader then
	vim.loader.enable()
end

-- load my configs
require("config").setup()
