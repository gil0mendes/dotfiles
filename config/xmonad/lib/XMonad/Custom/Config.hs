{-# OPTIONS_GHC -Wno-missing-signatures #-}

module XMonad.Custom.Config (
  myConfig,
) where

import Flow
import XMonad
import XMonad.Actions.DynamicProjects (dynamicProjects)
import XMonad.Actions.MostRecentlyUsed
import XMonad.Actions.Navigation2D (withNavigation2DConfig)
import XMonad.Core
import qualified XMonad.Custom.Bindings as C
import qualified XMonad.Custom.Hooks.Event as C
import qualified XMonad.Custom.Hooks.Layout as C
import qualified XMonad.Custom.Hooks.Log as C
import qualified XMonad.Custom.Hooks.Screens as C
import qualified XMonad.Custom.Hooks.Startup as C
import qualified XMonad.Custom.Hooks.Statusbar as C
import qualified XMonad.Custom.Manage.ManageHook as C
import qualified XMonad.Custom.Misc as C
import qualified XMonad.Custom.Navigation as C
import qualified XMonad.Custom.Theme as C
import qualified XMonad.Custom.Workspaces as C
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.Rescreen
import XMonad.Hooks.StatusBar
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.IndependentScreens
import qualified XMonad.Util.Hacks as Hacks

myConfig =
  def
    { borderWidth = C.border
    , workspaces = C.workspaces
    , layoutHook = C.layoutHook
    , terminal = C.term C.applications
    , normalBorderColor = C.colorN
    , focusedBorderColor = C.colorF
    , modMask = C.modMask
    , keys = C.myKeys
    , logHook = C.logHook
    , startupHook = C.startupHook
    , mouseBindings = C.mouseBindings
    , manageHook = C.manageHook
    , handleEventHook = C.handleEventHook
    , focusFollowsMouse = True
    , clickJustFocuses = False
    }
    |> dynamicProjects C.projects
    |> withNavigation2DConfig C.navigation
    |> withUrgencyHook (borderUrgencyHook C.red1)
    |> addRandrChangeHook C.myRandrChangeHook
    |> configureMRU
    |> ewmh
    |> ewmhFullscreen
    |> docks
    |> dynamicSBs C.barSpawner
    |> Hacks.javaHack
    |> (return :: a -> IO a)
