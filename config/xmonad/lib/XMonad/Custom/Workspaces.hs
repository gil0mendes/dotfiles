module XMonad.Custom.Workspaces where

import Data.Foldable
import Data.Maybe (Maybe (Just))
import Data.String (String)
import XMonad hiding (workspaces)
import XMonad.Actions.DynamicProjects
import XMonad.Actions.SpawnOn
import XMonad.Actions.WindowGo
import qualified XMonad.Custom.Misc as C
import XMonad.Custom.Prompt
import XMonad.Custom.Actions.Keyboard
import XMonad.Custom.Actions.ApplicationChooser

(generic, code, web, wsread, sys, tmp, vm) =
  ( "GEN"
  , "Code"
  , "WWW"
  , "Read"
  , "SYS"
  , "TMP"
  , "VM"
  )

workspaces :: [String]
workspaces = [generic, sys, tmp, code, web]

projects :: [Project]
projects =
  [ Project
      { projectName = generic
      , projectDirectory = "~/"
      , projectStartHook = Nothing
      }
  , Project
      { projectName = vm
      , projectDirectory = "~/"
      , projectStartHook = Just $ do
          spawnOn vm (C.virtualMachinesManger C.applications)
      }
  , Project
      { projectName = code
      , projectDirectory = "~/"
      , projectStartHook = Just raiseEditor
      }
  , Project
      { projectName = web
      , projectDirectory = "~/"
      , projectStartHook = Just $ do
          wrapKbdLayout $
            selectBrowserByNameAndDo
              promptTheme
              (spawnOn web)
      }
  , Project
      { projectName = sys
      , projectDirectory = "~/"
      , projectStartHook = Just $ do
          spawnOn sys (C.term C.applications)
          spawnOn sys (C.term C.applications)
      }
  , Project
      { projectName = tmp
      , projectDirectory = "~/"
      , projectStartHook = Nothing
      }
  ]
