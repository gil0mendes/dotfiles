module XMonad.Custom.Hooks.Startup (
  startupHook,
) where

import XMonad hiding (startupHook)
import XMonad.Hooks.SetWMName

startupHook :: X ()
startupHook = do
  setWMName "XMonad"
  spawn "betterlockscreen -w"
