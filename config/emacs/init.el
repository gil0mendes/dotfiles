  ;;;; About me
  (setq user-full-name "Gil Mendes"
        user-mail-address "gil00mendes@gmail.com")

  (setq create-lockfiles nil
        make-backup-files nil
        ;; in the case of needing to enable this some day, lets use
        ;; some sane defaults
        version-control t      ; number each backup file
        backup-by-copying t    ; instead of renaming current file
        delete-old-versions t  ; clean up after itself
        kept-old-versions 5
        kept-new-versions 5
        backup-directory-alist (list (cons "." (concat cache-dir "backup/"))))

(setq auto-save-default t
      ;; Don't auto-disable auto-save after deleting big chunks.
      auto-save-include-big-deletions t
      ;; put things on the correct directories
      auto-save-list-file-prefix (concat cache-dir "autosave/"))

  (when (native-comp-available-p)
    (setq native-compile-prune-cache t))

  ;; disable the damn thing by making it disposable.
  (setq custom-file (make-temp-file "emacs-custom-"))

  ;; Always start with *scratch*
  (setq initial-buffer-choice t)

  (mapc
   (lambda (string)
     (add-to-list 'load-path (locate-user-emacs-file string)))
   '("g0m-lisp" "g0m-emacs-modules"))

  ;;;; Packages
  (require 'package)

    (setq package-vs-register-as-project nil) ; Emacs 30

    (add-hook 'package-menu-mode-hook #'hl-line-mode)

    ;; Also read: <https://protesilaos.com/codelog/2022-05-13-emacs-elpa-devel/>
    (setq package-archives
        '(("gnu-elpa" . "https://elpa.gnu.org/packages/")
          ("gnu-elpa-devel" . "https://elpa.gnu.org/devel/")
          ("nongnu" . "https://elpa.nongnu.org/nongnu/")
          ("melpa" . "https://melpa.org/packages/")))

    ;; Highest number gets priority (what is not mentioned has priority 0)
    (setq package-archive-priorities
        '(("gnu-elpa" . 3)
          ("melpa" . 2)
          ("nongnu" . 1)))

(setq custom-safe-themes t)

(defmacro g0m-emacs-configure (&rest body)
  "Evaluate BODY as a `progn'.
BODY consists of ordinary Lisp expressions.  The sole exception
is an unquoted plist of the form (:delay NUMBER) which evaluates
BODY with NUMBER seconds of `run-with-timer'.

Note that `g0m-emacs-configure' does not try to autoload
anything.  Use it only for forms that evaluate regardless.

Also see `g0m-emacs-package'."
  (declare (indent 0))
  (let (delay)
    (dolist (element body)
      (when (plistp element)
        (pcase (car element)
          (:delay (setq delay (cadr element)
                        body (delq element body))))))
    (if delay
        `(run-with-timer ,delay nil (lambda () ,@body))
      `(progn ,@body))))

(defmacro g0m-emacs-keybind (keymap &rest definitions)
  "Expand key binding DEFINITIONS for the given KEYMAP.
DEFINITIONS is a sequence of string and command pairs."
  (declare (indent 1))
  (unless (zerop (% (length definitions) 2))
    (error "Uneven number of key+command pairs"))
  (let ((keys (seq-filter #'stringp definitions))
        ;; We do accept nil as a definition: it unsets the given key.
        (commands (seq-remove #'stringp definitions)))
    `(when-let (((keymapp ,keymap))
                (map ,keymap))
       ,@(mapcar
          (lambda (pair)
            (let* ((key (car pair))
                   (command (cdr pair)))
              (unless (and (null key) (null command))
                `(define-key map (kbd ,key) ,command))))
          (cl-mapcar #'cons keys commands)))))

;; Sample of `g0m-emacs-keybind'

;; (g0m-emacs-keybind global-map
;;   "C-z" nil
;;   "C-x b" #'switch-to-buffer
;;   "C-x C-c" nil
;; ;; Notice the -map as I am binding keymap here, not a command:
;;   "C-c b" beframe-prefix-map
;;   "C-x k" #'kill-buffer)

  (defun g0m-emacs-package-install (package &optional method)
  "Install PACKAGE with optional METHOD.

If METHOD is nil or the `builtin' symbol, PACKAGE is not
installed as it is considered part of Emacs.

If METHOD is a string, it must be a URL pointing to the version
controlled repository of PACKAGE.  Installation is done with
`package-vc-install'.

If METHOD is a quoted list, it must have a form accepted by
`package-vc-install' such as:

\\='(denote :url \"https://github.com/protesilaos/denote\" :branch \"main\")

If METHOD is any other non-nil value, install PACKAGE using
`package-install'."
  (unless (or (eq method 'builtin) (null method))
    (unless (package-installed-p package)
      (when (or (stringp method) (listp method))
        (package-vc-install method))
      (unless package-archive-contents
        (package-refresh-contents))
      (package-install package))))

(defvar g0m-emacs-loaded-packages nil)

(defmacro g0m-emacs-package (package &rest body)
  "Require PACKAGE with BODY configurations.

PACKAGE is an unquoted symbol that is passed to `require'.  It
thus conforms with `featurep'.

BODY consists of ordinary Lisp expressions.  There are,
nevertheless, two unquoted plists that are treated specially:

1. (:install METHOD)
2. (:delay NUMBER)

These plists can be anywhere in BODY and are not part of its
final expansion.

The :install property is the argument passed to
`g0m-emacs-package-install' and has the meaning of METHOD
described therein.

The :delay property makes the evaluation of PACKAGE with the
expanded BODY happen with `run-with-timer'.

Also see `g0m-emacs-configure'."
  (declare (indent defun))
    (let (install delay)
      (dolist (element body)
        (when (plistp element)
          (pcase (car element)
            (:install (setq install (cdr element)
                            body (delq element body)))
            (:delay (setq delay (cadr element)
                          body (delq element body))))))
      (let ((common `(,(when install
                         `(g0m-emacs-package-install ',package ,@install))
                      (require ',package)
                      (add-to-list 'g0m-emacs-loaded-packages ',package)
                      ,@body
                      ;; (message "Emacs loaded package: %s" ',package)
                      )))
        (cond
         ((featurep package)
          `(progn ,@body))
         (delay
          `(run-with-timer ,delay nil (lambda () ,@(delq nil common))))
         (t
          `(progn ,@(delq nil common)))))))

  (require 'g0m-emacs-theme)
  (require 'g0m-emacs-essentials)
  (require 'g0m-emacs-org)
  (require 'g0m-emacs-which-key)
  (require 'g0m-emacs-window)
  (require 'g0m-emacs-completion)
  (require 'g0m-emacs-langs)
  (require 'g0m-emacs-git)
  (require 'g0m-emacs-icons)
