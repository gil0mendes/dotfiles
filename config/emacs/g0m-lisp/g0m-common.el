;; This file contains some generic macros and functions that I use
;; thoughtout my Emacs configuration

(defmacro cmd!! (command &optional prefix-arg &rest args)
  "Returns a closure that interactively calls COMMAND with ARGS and
PREFIX-ARG. Like `cmd!', but allows you to change `current-prefix-arg'
or pass arguments to COMMAND. This macro is meant to be used as a
target for keybinds (e.g. with `define-key' or `map!')."
  (declare (doc-string 1) (pure t) (side-effect-free t))
  `(lambda (arg &rest _) (interactive "p")
	 (let ((current-prefix-arg (or ,prefix-arg arg)))
	   (,(if args #'funcall-interactively #'call-interactively)
		,command
		,@args))))

(defmacro g0m-pushnew (place &rest values)
  "Push VALUES sequentially into PLACE, if they aren't already present.
This is a variadic `cl-pushnew'."
  (let ((var (make-symbol "result")))
    `(dolist (,var (list ,@values) (with-no-warnings ,place))
       (cl-pushnew ,var ,place :test #'equal))))

(provide 'g0m-common)
