;;; g0m-prefix.el --- Prefix keymap for my dotemacs -*- lexical-binding: t -*-

;; Prefix keymap for my custom keymaps

(defvar-keymap g0m-prefix-buffer-map
  :doc "Prefix keymap for buffers."
  :name "Buffer"
  :prefix 'g0m-prefix-buffer
  "[" (cons "Previous buffer" #'previous-buffer)
  "]" (cons "Next buffer" #'next-buffer)
  "p" (cons "Previous buffer" #'previous-buffer)
  "n" (cons "Next buffer" #'next-buffer)
  "b" (cons "Switch buffer" #'consult-buffer)
  "i" #'ibuffer
  "k" (cons "Kill current buffer" #'kill-current-buffer)
  "r" (cons "Rename buffer" #'rename-buffer))

(defvar-keymap g0m-prefix-file-map
  :doc "Prefix keymap for files."
  :name "File"
  :prefix 'g0m-prefix-file
  "D" (cons "Find directory" #'dired)
  "f" (cons "Find file" #'find-file)
  "F" (cons "Find file other window" #'find-file-other-window)
  "s" (cons "Save file" #'save-buffer)
  "S" (cons "Save file as..." #'write-file))

(defvar-keymap g0m-prefix-search
  :doc "Prefix keymap for search."
  :name "Search"
  :prefix 'g0m-prefix-search
  "b" (cons "Search buffer" #'consult-line)
  "B" (cons "Search all open buffers" (cmd!! #'consult-line-multi 'all-buffers)))

(defvar-keymap g0m-prefix-mode-map
  :doc "Prefix keymap for minor mode toggles."
  :name "Toggle"
  :prefix 'g0m-prefix-mode
  "f" #'flymake-mode
  "h" #'hl-line-mode
  "m" #'menu-bar-mode
  "n" #'display-line-numbers-mode
  "t" #'toggle-truncate-lines
  "s" #'spacious-padding-mode)

(defvar-keymap g0m-prefix-code-map
  :doc "Prefix keymap for code actions."
  :name "Code"
  :prefix 'g0m-prefix-code
  "a" (cons "LSP Execute code action" #'eglot-code-actions)
  "r" (cons "LSP Rename" #'eglot-rename)
  "j" (cons "LSP Find declaration" #'eglot-rename))

(defvar-keymap g0m-prefix-quit-map
  :doc "Prefix keymap for quit"
  :name "Quit/Restart"
  :prefix 'g0m-prefix-quit
  "f" (cons "Delete frame" #'delete-frame)
  "K" (cons "Kill Emacs (and daemon)" #'save-buffers-kill-emacs)
  "q" (cons "Quit Emacs" #'kill-emacs))
 
(defvar-keymap g0m-prefix-map
  :doc "Prefix keymap with multiple subkeymaps"
  :name "G0M Prefix"
  :prefix 'g0m-prefix
  "0" (cons "Delete window" #'delete-window)
  "b" (cons "Buffer" 'g0m-prefix-buffer)
  "f" (cons "File" 'g0m-prefix-file)
  "c" (cons "Code" 'g0m-prefix-code)
  "t" (cons "Toggle" 'g0m-prefix-mode)
  "s" (cons "Search" 'g0m-prefix-search)
  "q" (cons "Quit/Restart" 'g0m-prefix-quit))

(provide 'g0m-prefix)
