  (setq frame-resize-pixelwise t
	frame-inhibit-implied-resize t
	frame-title-format '("%b")
	ring-bell-function 'ignore
	use-dialog-box t ; only for mouse events, which I try to not use much
	use-file-dialog nil
	use-short-answers t
	inhibit-splash-screen t
	inhibit-startup-screen t
	inhibit-x-resources t
	inhibit-startup-echo-area-message user-login-name ; read the docstring
	inhibit-startup-buffer-menu t)

  ;; I do not use those graphical elements by default
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)

  ;;;; Cache
  (eval-and-compile
    (defvar data-dir
      (if (getenv "XDG_DATA_HOME")
          (concat (getenv "XDG_DATA_HOME") "/emacs/")
        (expand-file-name "~/.local/share/emacs/"))
      "Directory for data.")

    (defvar cache-dir
      (if (getenv "XDG_CACHE_HOME")
          (concat (getenv "XDG_CACHE_HOME") "/emacs/")
        (expand-file-name "~/.cache/emacs/"))
      "Directory for cache.")

    (defvar pictures-dir
      (or (getenv "XDG_PICTURES_HOME")
        (expand-file-name "~/Pictures/"))
      "Directory for pictures."))

  (setq gc-cons-threshold most-positive-fixnum
        gc-cons-percentage 0.5)
  (defvar g0m-emacs--file-name-handler-alist file-name-handler-alist)
  (defvar g0m-emacs--vc-handled-backends vc-handled-backends)

  (setq file-name-handler-alist nil
        vc-handled-backends nil)

  (add-hook 'emacs-startup-hook
            (lambda ()
              (setq gc-cons-threshold (* 1000 1000 8)
                    gc-cons-percentage 0.1
                    file-name-handler-alist g0m-emacs--file-name-handler-alist
                    vc-handled-backends g0m-emacs--vc-handled-backends)))

  ;;;; Display the statup time after load
  (add-hook 'emacs-startup-hook
            (lambda ()
              (message "🚀 Loaded Emacs in %.03fs"
                       (float-time (time-subtract after-init-time before-init-time)))))

  ;;;; General theme code

  (defun g0m-emacs-theme-environment-dark-p ()
    "Return non-nil if gsettings (GNOME) has a dark theme."
    (string-match-p
     "dark"
     (shell-command-to-string "gsettings get org.gnome.desktop.interface color-scheme")))

  (defun g0m-emacs-re-enable-frame-theme (_frame)
    "Re-enable active theme, if any, upn FRAME creation.
  Add this to `after-make-frame-functions' so that new frames do not retain the generic background
  set by the function `g0m-emacs-avoid-initial-flash-or-light'."
    (when-let ((theme (car custom-enabled-themes)))
      (enable-theme theme)))

  (defun g0m-emacs-avoid-initial-flash-of-light ()
    "Avoid flash of light when stating Emacs, if needed."
    (when (g0m-emacs-theme-environment-dark-p)
      (setq mode-line-format nil)
      (set-face-attribute 'default nil :background "#000000" :foreground "#ffffff")
      (set-face-attribute 'mode-line nil :background "#000000" :foreground "#ffffff")
      (add-hook 'after-make-frame-functions #'g0m-emacs-re-enable-frame-theme)))

  ;; (g0m-emacs-avoid-initial-flash-of-light)
