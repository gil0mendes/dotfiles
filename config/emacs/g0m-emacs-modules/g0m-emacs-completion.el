;;; General minibuffer settings
(g0m-emacs-configure
  (:delay 1)

  ;;;; Minibuffer configuration
  (setq completion-styles '(basic substring initials flex orderless)))

(g0m-emacs-configure
  (:delay 5)
  ;; Reset all the per-category defaults so that (i) we use the
  ;; standard `completion-styles' and (ii) can specify our own styles
  ;; in the `completion-category-overrides' without having to
  ;; explicitly override everything.
  (setq completion-category-defaults nil)

  (setq completion-category-overrides
        ;; NOTE: I added `basic' because it works better as a
        ;; default for some contexts.  Read:
        ;; <https://debbugs.gnu.org/cgi/bugreport.cgi?bug=50387>.
        ;;
        ;; `partial-completion' is a killer app for files, because it
        ;; can expand ~/.l/s/fo to ~/.local/share/fonts.
        ;;
        ;; If `basic' cannot match my current input, Emacs tries the
        ;; next completion style in the given order.  In other words,
        ;; `orderless' kicks in as soon as I input a space or one of its
        ;; style dispatcher characters.
        '((file (styles . (basic partial-completion orderless)))
          (bookmark (styles . (basic substring)))
          (library (styles . (basic substring)))
          (embark-keybinding (styles . (basic substring)))
          (imenu (styles . (basic substring orderless)))
          (consult-location (styles . (basic substring orderless)))
          (kill-ring (styles . (emacs22 orderless)))
          (eglot (styles . (emacs22 substring orderless))))))

(g0m-emacs-configure
  (:delay 5)
  (setq completion-ignore-case t)
  (setq read-buffer-completion-ignore-case t)
  (setq-default case-fold-search t) ; for general regexp
  (setq read-file-name-completion-ignore-case t))

(g0m-emacs-configure
  (:delay 5)
  (setq enable-recursive-minibuffers t)
  (setq read-minibuffer-restore-windows nil)
  (minibuffer-depth-indicate-mode 1))

(g0m-emacs-configure
  (:delay 5)
  (setq minibuffer-default-prompt-format " [%s]")
  (minibuffer-electric-default-mode 1))

(g0m-emacs-configure
  (:delay 5)
  (setq resize-mini-windows t)
  (setq read-answer-short t) ; also check `use-short-answers' for Emacs28
  (setq echo-keystrokes 0.25)
  (setq kill-ring-max 60) ; Keep it small

  ;; Do not allow the cursor to move inside the minibuffer prompt.  I
  ;; got this from the documentation of Daniel Mendler's Vertico
  ;; package: <https://github.com/minad/vertico>.
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))

  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  (file-name-shadow-mode 1))

(g0m-emacs-package savehist
  (:delay 1)
  (setq savehist-file (locate-user-emacs-file "savehist"))
  (setq history-length 100)
  (setq history-delete-duplicates t)
  (setq savehist-save-minibiffer-history t)
  (setq savehist-additional-variables '(register-alist kill-ring))
  (savehist-mode 1))

(g0m-emacs-package orderless
  (:install t)
  (:delay 5)
  ;; Remember to check my `completion-styles' and the
  ;; `completion-category-overrides'.
  (setq orderless-matching-styles
        '(orderless-prefixes orderless-regexp orderless-flex orderless-initialism))

  ;; SPC should never complete: use it for `orderless' groups.
  ;; The `?' is a regexp construct.
  (g0m-emacs-keybind minibuffer-local-completion-map
    "SPC" nil
    "?" nil))

;;; Corfu (in-buffer completion popup)
(g0m-emacs-package corfu
  (:install t)
  (:delay 5)

  (setq corfu-cycle t
		corfu-count 16
		corfu-max-width 120
		corfu-min-width 20
		corfu-on-exact-match nil)
  (setq corfu-preview-current nil)

  (global-corfu-mode 1)

  (setq corfu-popupinfo-delay '(1.25 . 0.5))
  (corfu-popupinfo-mode 1) ; show documentation after `corfu-popupinfo-delay'

  ;; (define-key corfu-map (kbd "<tab>") #'corfu-complete)

  ;; Sort by input history (no need to modify `corfu-sort-function').
  (with-eval-after-load 'savehist
    (corfu-history-mode 1)
    (add-to-list 'savehist-additional-variables 'corfu-history)))

;;; Enhanced minibuffer commands
(g0m-emacs-package consult
  (:install t)
  (:delay 5)
  (setq consult-line-numbers-widen t)
  (setq consult-async-min-input 2)
  (setq consult-async-refresh-delay 0.15)
  (setq consult-async-input-debounce 0.1)
  (setq consult-async-input-throttle 0.2)
  (setq consult-narrow-key nil)
  (setq consult-find-args (concat "find . -not ( "
				  "-path */.git* -prune "
				  "-or -path */.cache* -prune )"))
  (setq consult-preview-key 'any)

  (add-to-list 'consult-mode-histories '(vc-git-log-edit-mode . log-edit-comment-ring))

  (add-hook 'completion-list-mode-hook #'consult-preview-at-proint-mode)

  ;; TODO: add imenu

  (g0m-emacs-keybind global-map
    ("M-g M-g" #'consult-goto-line)
    ("C-x b" #'consult-buffer))
  ;; TODO: add global keybinds
  ;; TODO: add pulsar support
  )

(g0m-emacs-package consult-dir
  (:install t)
  (:delay 5)

  (g0m-emacs-keybind global-map
    "C-x C-d" #'consult-dir)

  (g0m-emacs-keybind minibuffer-local-completion-map
    "C-x C-d" #'consult-dir
    "C-x C-j" #'consult-dir-jump-file))

;;; Detailed completion annotations
(g0m-emacs-package marginalia
  (:install t)
  (:delay 5)
  (setq marginalia-max-relative-age 0)
  (marginalia-mode 1))

(g0m-emacs-package vertico
  (:install t)
  (:delay 1)
  (setq vertico-scroll-margin 0)
  (setq vertico-count 15)
  (setq vertico-resize nil)
  (setq vertico-cycle t)

  ;; Cleans up path when moving directories with shadowed paths syntax, e.g.
  ;; cleans ~/foo/bar/// to /, and ~/foo/bar/~/ to ~/.
  (add-hook 'minibuffer-setup-hook #'vertico-repeat-save)

  (vertico-mode 1))

(provide 'g0m-emacs-completion)
