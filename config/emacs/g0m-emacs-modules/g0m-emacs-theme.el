  ;;;; Theme setup and related

  ;;; The Ef themes
  (g0m-emacs-package ef-themes
                      (:install t)
                      (:delay 1)
                      (setq ef-theme-variable-pitch-ui t
                            ef-themes-mixed-fonts t))

  (setq g0m-theme nil)

  (defun g0m-auto-update-theme ()
    "Depending on time use different theme"
    (let* ((hour (nth 2 (decode-time (current-time))))
           (theme (cond ((<= 7 hour 8)   'ef-arbutus)
                        ((= 9 hour)      'ef-day)
                        ((<= 10 hour 16) 'ef-spring)
                        ((<= 17 hour 18) 'ef-melissa-light)
                        ((<= 19 hour 22) 'ef-duo-dark)
                        (t               'ef-winter))))
      (when (not (equal g0m-theme theme))
        (setq g0m-theme theme)
        (load-theme g0m-theme t))

      ;; run this function again next hour
      (run-at-time (format "%02d:%02d" (+ hour 1) 0) nil 'g0m-auto-update-theme)))

  ;; run the function to load the right theme
  (g0m-auto-update-theme)

      ;;;; Pulsar
    (g0m-emacs-package pulsar
      (:install t)
      (:delay 1)
      (setopt pulsar-pulse t
              pulsar-delay 0.055
              pulsar-iterations 10
              pulsar-face 'pulsar-magenta
              pulsar-highlight-face 'pulsar-cyan)

      (pulsar-global-mode 1)

      ;; There are convenience functions/commands which pulse the line using
      ;; a specific colour: `pulsar-pulse-line-red' is one of them.
      (add-hook 'next-error-hook #'pulsar-pulse-line-red)
      (add-hook 'next-error-hook #'pulsar-recenter-top)
      (add-hook 'next-error-hook #'pulsar-reveal-entry)

      (add-hook 'minibuffer-setup-hook #'pulsar-pulse-line-red))

  (g0m-emacs-package lin
    (:install t)
    (:delay 1)
    (setq lin-face 'lin-magenta)

    (lin-global-mode 1))

    (g0m-emacs-package spacious-padding
      (:install t)
      (:delay 1)

      ;; These are the defaults, but lets keep it here for visibility
      (setq spacious-padding-widths
            '( :internal-border-width 15
               :header-line-width 4
               :mode-line-width 6
               :tab-width 4
               :right-divider-width 1
               :scroll-bar-width 8
               :left-fringe-width 20
               :right-fringe-width 20))

      (setq spacious-padding-subtle-mode-line nil)

      (spacious-padding-mode 1))

  (provide 'g0m-emacs-theme)
