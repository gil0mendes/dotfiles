  ;;; Org-mode (personal information manager)
  ;; Some of these settings need to be evaluated before the `org` feature is loaded, so I am
  ;; `g0m-emacs-configure` and then simply `require` the feature at the right spot.
  (g0m-emacs-configure
    (:delay 5)
    (setq org-export-backends '(html texinfo md))

    (setq org-directory (expand-file-name "~/Documents/org/"))
    (setq org-imenu-depth 7)

    ;;;; general settings
    (setq org-ellipsis "⮧")
    (setq org-adapt-indentation nil)
    (setq org-special-ctrl-a/e nil)
    (setq org-special-ctrl-k nil)
    (setq org-M-RET-may-split-line '((default . nil)))
    (setq org-hide-emphasis-markers nil)
    (setq org-hide-macro-markers nil)
    (setq org-hide-leading-stars nil)
    (setq org-cycle-separator-lines 0)
    (setq org-structure-template-alist
        '(("s" . "src")
          ("e" . "src emacs-lisp")
          ("E" . "src emacs-lisp :results value code :lexical t")
          ("t" . "src emacs-lisp :tangle FILENAME")
          ("T" . "src emacs-lisp :tangle FILENAME :mkdirp yes")
          ("x" . "example")
          ("X" . "export")
          ("q" . "quote")))
    (setq org-catch-invisible-edits 'show)
    (setq org-return-follows-link nil)
    (setq org-loop-over-headlines-in-active-region 'start-level)
    (setq org-modules '(ol-info ol-eww))
    (setq org-use-sub-superscripts '{})
    (setq org-insert-heading-respect-content t)
    (setq org-read-date-prefer-future 'time))

  (g0m-emacs-package org-modern
    (:install t)
    (:delay 5)

    (add-hook 'org-mode-hook #'org-modern-mode))

  ;;;; code
  (g0m-emacs-configure
      (:delay 5)
      (setq org-confirm-babel-evaluate nil)
      (setq org-src-window-setup 'current-window)
      (setq org-edit-src-persistent-message nil)
      (setq org-src-fontify-natively t)
      (setq org-src-preserve-indentation t)
      (setq org-src-tab-acts-natively t)
      (setq org-edit-src-content-indication 0))

  (g0m-emacs-configure
    (:delay 5)
    (setq org-export-with-toc t)
    (setq org-export-headline-levels 8)
    (setq org-export-dispatch-use-expert-ui nil)
    (setq org-html-htmlize-output-type nil)
    (setq org-html-head-include-default-style nil)
    (setq org-html-head-include-scripts nil))

  (g0m-emacs-configure
    (:delay 5)
    (setq org-id-link-ro-org-use-id 'create-if-interactive-and-no-custom-id))

  (provide 'g0m-emacs-org)
