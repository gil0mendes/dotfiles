  ;;; Essential configurations
  (g0m-emacs-configure
     (:delay 1)

     ;;;; General settings
     (setq blink-matching-paren nil)
     (setq delete-pair-blink-delay 0.1)
     (setq help-window-select t)
     (setq next-error-recenter '(4)) ; center of the window
     (setq find-library-include-other-files nil)
     (setq tramp-connection-timeout (* 60 10)) ; seconds
     (setq save-interprogram-paste-before-kill t)
     (setq mode-require-final-newline 'visit-save)
     (setq-default truncate-partial-width-windows nil)
     (setq eval-expression-print-length nil)
     (setq kill-do-not-save-duplicates t)
     (setq duplicate-line-final-position -1
           duplicate-region-final-position -1)
     (setq scroll-error-top-bottom t))

(use-package g0m-common
  :ensure nil)

  ;;;; rule indicator
  (g0m-emacs-configure
    (:delay 1)
    (setq display-fill-column-indicator-column 119)
    (global-display-fill-column-indicator-mode 1))

;;;; Prefix keymap
(use-package g0m-prefix
  :ensure nil
  :bind-keymap
  (("M-SPC" . g0m-prefix)
   ("C-z" . g0m-prefix)))

  ;;;; Mouse and mouse wheel behaviour
  (g0m-emacs-configure
    (:delay 5)
    ;; Scrolling behaviour
    (setq-default scroll-preserve-screen-position t
                  scroll-conservatively 1 ; affects `scroll-step`
                  scroll-margin 0
                  next-screen-context-lines 0)

    (mouse-wheel-mode 1))

  ;;;; Auto revert mode
  (g0m-emacs-configure
    (:delay 5)
    (setq auto-revert-verbose t)
    (global-auto-revert-mode 1))

  ;;;; Delete selection
  (g0m-emacs-configure
    (:delay 5)
    (delete-selection-mode 1))

  (defun g0m/backward-delete-word (arg)
    "Delete characteres backward until encountering the beginning
  of a work. With argument ARG, do this that many times."
    (interactive "p")
    (delete-region (point) (progn (backward-word arg) (point))))

  (defun g0m/backward-delete-line (arg)
    "Delete (not kill) the current line, backwards from cursor.
  With argument ARG, do this that many times."
    (interactive "p")
    (delete-region (point) (progn (beginning-of-visual-line arg) (point))))

  (g0m-emacs-keybind global-map
    "M-<delete>" #'g0m/backward-delete-word
    "M-<backspace>" #'g0m/backward-delete-line)

  ;;;; Work clock
  (g0m-emacs-configure
    (:delay 5)
    (setq display-time-world-list t)
    (setq world-clock-list ; M-x shell RET timedatectl list-timezones
          '(("America/Los_Angeles" "Pacific")
            ("America/Sao_Paulo" "Sao Paulo")
            ("Europe/Lisbon" "Lisbon")
            ("UTC" "UTC"))))

  ;;;; `proced` (process monitor, similar to `top`)
  (g0m-emacs-configure
    (:delay 10)
    (setq proced-auto-update-flag t)
    (setq proced-enable-color-flag t)
    (setq proced-auto-update-interval 5)
    (setq proced-descent t)
    (setq proced-filter 'user))

  ;;;; Emacs server (allow emacsclient to connect to running session)
  (g0m-emacs-configure
    (:delay 1)
    (require 'server)
    (setq server-client-intructions nil)
    (unless (server-running-p)
      (server-start)))

;;;; project
(use-package project
  :ensure nil
  :bind
  (("C-x p ." . project-dired)
   ("C-x p C-g" . keyboard-quit)
   ("C-x p <return>" . project-dired)
   ("C-x p <delete>" . project-forget-project))
  :config
  (setopt project-switch-commands
          '((project-find-file "Find file")
            (project-find-regexp "Find regexp")
            (project-find-dir "Find directory")
            (project-dired "Root dired")
            (project-vc-dir "VC-Dir")
            (project-shell "Shell")
            (keyboard-quit "Quit")))
  (setq project-vc-extra-root-markers '(".project"))
  (setq project-key-prompt-style t))

  (provide 'g0m-emacs-essentials)
