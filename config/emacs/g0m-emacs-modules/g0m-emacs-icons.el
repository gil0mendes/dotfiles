;;;; Icons
(g0m-emacs-package nerd-icons
  (:install t)
  (:delay 5))

(g0m-emacs-package nerd-icons-completion
  (:install t)
  (:delay 5)
  (nerd-icons-completion-marginalia-setup)
  (nerd-icons-completion-mode 1))

(g0m-emacs-package nerd-icons-corfu
  (:install t)
  (:delay 5)
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(g0m-emacs-package nerd-icons-dired
  (:install t)
  (:delay 5)
  (add-hook 'dired-mode-hook #'nerd-icons-dired-mode))

(provide 'g0m-emacs-icons)
