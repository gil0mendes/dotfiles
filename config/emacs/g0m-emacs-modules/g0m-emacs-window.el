;;; General window and buffer configuration
(g0m-emacs-configure
  (:delay 1)

;;;; Line highlight
(require 'hl-line)
(setq hl-line-sticky-flag nil)
(hl-line-mode 1))

(provide 'g0m-emacs-window)
