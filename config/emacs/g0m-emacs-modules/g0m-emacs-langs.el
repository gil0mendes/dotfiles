(g0m-emacs-package move-text
  (:install t)
  (:delay 10)

  ;; define keymap
  (g0m-emacs-keybind global-map
    "M-<down>" #'move-text-down
    "M-<up>" #'move-text-up))

;;;; Tabs, indentation, and the TAB key
(g0m-emacs-configure
  (:delay 2)

  ;; sorry people, but I like tabs instead of spaces and 4-space
  ;; tabs is what makes sense to me. Either way, this can be changed
  ;; on a per-mode basis.
  (setq-default indent-tabs-mode t
                tab-width 4)

  ;; (setq tab-first-completion 'word-or-paren-or-punct)
  (setq tab-always-indent 'complete))

;;;; Parentheses
(g0m-emacs-configure
  (:delay 2)

  (setq show-paren-style 'parenthesis)
  (setq show-paren-when-point-in-periphery nil)
  (setq show-paren-when-point-inside-paren nil)
  (setq show-paren-context-when-offscreen 'overlay)
  (add-hook 'after-init-hook #'show-paren-mode))

;;;; Eldoc (Emacs live documentation feedback)
(g0m-emacs-configure
  (:delay 2)
  (setq eldoc-message-function #'message) ; don't use mode line for M-x eval-expression, etc.
  (add-hook 'prog-mode-hook #'eldoc-mode))

;;;; Handle performance for very long lines
(g0m-emacs-configure
  (:delay 2)
  (global-so-long-mode 1))

;;;; tree-sitter configuration
(g0m-emacs-package treesit
  (:delay 2)
  ;; list of languages that we want to support
  (setq treesit-language-source-alist
        '((bash "https://github.com/tree-sitter/tree-sitter-bash")
          (css "https://github.com/tree-sitter/tree-sitter-css" "v0.20.0")
          (elisp "https://github.com/wilfred/tree-sitter-elisp")
          (go "https://github.com/tree-sitter/tree-sitter-go")
          (html "https://github.com/tree-sitter/tree-sitter-html")
          (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
          (json "https://github.com/tree-sitter/tree-sitter-json")
          (make "https://github.com/alemuller/tree-sitter-make")
          (markdown "https://github.com/ikatyang/tree-sitter-markdown")
          (nix "https://github.com/nix-community/tree-sitter-nix")
          (python "https://github.com/tree-sitter/tree-sitter-python")
          (toml "https://github.com/tree-sitter/tree-sitter-toml")
          (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
          (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
          (yaml "https://github.com/ikatyang/tree-sitter-yaml")))
  ;; install all of the packages in one go
  (defun g0m/treesit-install-all-languages ()
    "install all languages specified by `treesit-language-source-alist'"
    (interactive)
    (let ((languages (mapcar 'car treesit-language-source-alist)))
      (dolist (lang languages)
          (treesit-install-language-grammar lang)
          (message "🤘 `%s' parse was installed." lang)
          (sit-for 0.75)))))

(g0m-emacs-package eglot
  (:delay 2)
  (setq eglot-sync-connect 1)
  (setq eglot-autoshutdown t)
  
  (add-hook 'prog-mode-hook
            (lambda ()
              (add-hook 'before-save-hook 'eglot-format nil t))))

(g0m-emacs-package consult-eglot
  (:install t)
  (:delay 2))

;;;; Markdown (markdown-mode)
(g0m-emacs-package markdown-mode
  (:install t)
  (:delay 5)
  (setq markdown-fontify-code-blocks-natively t))

(g0m-emacs-package jinx
  (:install t)
  (:delay 10)
  (setq jinx-languages "en_US pt_PT")
  (setq jinx-include-modes '(text-mode prog-mode))
  (setq jinx-include-faces
        '((prog-mode font-lock-doc-face)
          (conf-mode font-lock-comment-face)))

  (define-key ctl-x-x-map "j" #'jinx-mode) ; C-x x j

  (g0m-emacs-keybind global-map
    "M-$" #'jinx-correct
    "C-M-$" #'jinx-languages))

;;;; Flymake
(g0m-emacs-package flymake
  (:delay 30)
  (setq flymake-fringe-indicator-position 'left-fringe)
  (setq flymake-suppress-zero-counters t)
  (setq flymake-no-changes-timeout nil)
  (setq flymake-start-on-flymake-mode t)
  (setq flymake-start-on-save-buffer t)
  (setq flymake-proc-complication-prevents-syntax-check t)
  (setq flymake-wrap-around nil)
  (setq flymake-mode-line-format
        '("" flymake-mode-line-exception flymake-mode-line-counters))
  (setq flymake-show-diagnostics-at-end-of-line nil)


  (defvar g0m/flymake-mode-projects-path
    (file-name-as-directory (expand-file-name "~/projects/"))
    "Path to my Git projects.")

  (defun g0m/flymake-mode-lexical-binding ()
    (when lexical-binding
      (flymake-mode 1)))

  (defun g0m/flymake-mode-in-my-projects ()
    (when-let ((file (buffer-file-name))
               ((string-prefix-p g0m/flymake-mode-projects-path
                                (expand-file-name file)))
               ((not (file-directory-p file)))
               ((file-regular-p file)))
      (add-hook 'find-file-hook #'g0m/flymake-mode-lexical-binding nil t)))

  (add-hook 'emacs-lisp-mode-hook #'g0m/flymake-mode-in-my-projects)

  (define-key ctl-x-x-map "m" #'flymake-mode) ; C-x x m
  (g0m-emacs-keybind flymake-mode-map
    "C-c ! s" #'flymake-start
    "C-c ! d" #'flymake-show-buffer-diagnostics
    "C-c ! D" #'flymake-show-project-diagnostics
    "C-c ! n" #'flymake-goto-next-error
    "C-c ! p" #'flymake-goto-prev-error))

;;;; JavaScript & TypeScript
(g0m-emacs-package typescript-mode
  (:delay 5)
  (add-hook 'typescript-mode #'eglot-ensure))
;;;; Nix
(g0m-emacs-package nix-mode
  (:delay 5)

  ;; make emacs auto load nix-mode with .nix files
  (add-to-list 'auto-mode-alist '("\\.nix\\'" . nix-mode))

  ;; load eglot when nix-mode is enabled
  (add-hook 'nix-mode #'eglot-ensure)

  ;; configure key maps
  (g0m-emacs-keybind global-map
    "C-c" nix-mode-map))

;;;; Rust
(g0m-emacs-package rustic
  (:install t)
  (:delay 5)

  ;; make emacs auto load rustic-mode with .rs files
  (add-to-list 'auto-mode-alist '("\\.rs$" . rustic-mode))

  ;; TODO: add alias to rust mode

  (setq rustic-indent-method-chain t)

  ;; configure LSP
  (setq rustic-lsp-client 'eglot)
  (add-hook 'rustic-mode-local-vars-hook #'rustic-setup-lsp 'append))

(provide 'g0m-emacs-langs)
