# gil0mendes's dotfiles

These are my personal dotfiles not only for my OS, but also for almost all the software that I use. This document also works like a kind of manual for what is possible to do with this configuration.

# After Install

## SSH Agent

Open 1Password enable the CLI and the SSH Agent integration, then the following config on the `~/.ssh/config` file.

```text
Host *
	IdentityAgent ~/.1password/agent.sock
```

# Manual

## Shell

The main shell is the ZSH one, but its possible to have multiple ones.

This are some of the packages that compose the shell:

- [nix-zsh-completions](https://github.com/nix-community/nix-zsh-completions), for completion on Nix commands;
- [bat](https://github.com/sharkdp/bat), as a cat replacement. This adds a bit of color to our prints;
- [eza](https://github.com/eza-community/eza), for a modern `ls` replacement;
- [fd](https://github.com/sharkdp/fd), for a simple, fast and user-friendly alternative to `find`;
- [fzf](https://github.com/junegunn/fzf), as out fuzzy finder on the console;
- [jq](https://github.com/jqlang/jq), for parsing JSON on the command-line. Btw, this allows apply queries to the data;
- [ripgrep](https://github.com/BurntSushi/ripgrep), for recursively search directories for a regex pattern;
- [tldr](https://github.com/tldr-pages/tldr), fast cheatsheets for commands;

## Security

### Fingerprint

To add a new fingerprint run:

```sh
fprint-enroll $username
```

To verify the fingerprint run:

```sh
fprint-verify
```

# Maintenance

To remove old generations:

```sh
sudo nix profile wipe-history --profile /nix/var/nix/profiles/system --older-than 14d
```

# Resources

- https://github.com/hlissner/dotfiles/tree/master
