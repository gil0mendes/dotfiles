{ config, lib, pkgs, ... }:

with lib; {
  # hardware related stuff
  hardware.enableRedistributableFirmware = true;

  # Configure localization
  time.timeZone = mkDefault "Europe/Lisbon";
  i18n.defaultLocale = mkDefault "en_US.UTF-8";
  location.provider = "geoclue2";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
}


