{ config, pkgs, ... }:

{
  imports =
    [
      ../personal.nix
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.loader.grub = {
    enable = true;
    efiSupport = true;
    enableCryptodisk = true;
    device = "nodev";
  };

  boot.initrd.luks.devices = {
    enc = {
      device = "/dev/disk/by-uuid/4c7e4c65-d4a3-42dc-a1ab-28aa8b529532";
      preLVM = true;
    };
  };

  boot.kernelParams = [ "processor.maxcstate=4" ];
  boot.kernelPackages = pkgs.linuxPackages_latest;

  ## Modules
  modules = {
    system = {
      diagnostics = {
        enable = true;
        benchmarks.enable = true;
      };
    };
    desktop = {
      xorg = {
        gnome.enable = true;
      };
      apps = {
        onePassword.enable = true;
        thunderbird.enable = true;
        chat.enable = true;
      };
      browsers = {
        default = "firefox";
        firefox.enable = true;
      };
      media = {
        documents = {
          enable = true;
          pdf.enable = true;
          ebook.enable = true;
          office.enable = true;
        };
        graphics.enable = true;
        video.enable = true;
        recording.enable = true;
        spotify.enable = true;
      };
      term = {
        default = "kitty";
        kitty.enable = true;
      };
      vm = {
        virtualbox.enable = true;
        qemu.enable = true;
      };
    };
    dev = {
      cc.enable = true;
      xdg.enable = true;
      haskell.enable = true;
      node.enable = true;
      rust.enable = true;
      python.enable = true;
      java.enable = true;
      nix.enable = true;
    };
    editors = {
      default = "nvim";
      vscode.enable = true;
      vim.enable = true;
      emacs.enable = true;
    };
    shell = {
      direnv.enable = true;
      git = {
        enable = true;
        gui = true;
      };
      zsh.enable = true;
      gnupg.enable = true;
    };
    services = {
      docker.enable = true;
      ssh.enable = true;
      tlp.enable = true;
      flatpak.enable = true;
      kanata.enable = true;
    };
    theme.active = "dark-momentum";
  };

  # Local config
  programs.ssh.startAgent = true;
  services.openssh.startWhenNeeded = true;

  networking = {
    hostName = "eezo";
    nameservers = [
      "1.1.1.1"
      "1.0.0.1"
      "2606:4700:4700::1111"
      "2606:4700:4700::1001"
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # editors
    vim
    emacs

    # dev tools
    wget
    nixpkgs-fmt
    openssl
    pkg-config
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
